#ifndef _IMP_VFD_DETECT_H_
#define _IMP_VFD_DETECT_H_

#include "imp_vfd_cascade.h"
#include"imp_vfd_track.h"

#define VFD_TIME_LIMIT 12 * 3600 * 5

#ifdef __cplusplus
extern "C"
{
#endif
	static float fTbs[] = { 1.3f, 1.2f, 1.1f, 1.08f, 1.06f, 1.03f };
	static int tTbs[] = { 5, 10, 20, 50, 100, 200 };
	static int sTbs[] = { 1, 2, 3, 4, 5, 6 };
	static int cTbs[] = { 0, 1, 2, 3, 5, 7 };
	static int threshTbs[] = {0, 3, 5, 10,20,40};

	typedef struct impFACE_PROP_Internal_S
	{
		IMP_RECT_S stPosition;		/* Position of human face */
		IMP_S32 s32Facesize;		/* Face size (in pixels) */
		IMP_S32 u32FaceID;			/* Face ID*/
		IMP_U8 flagShow;
		float angle;				/* face roll angle*/

		IMP_ALIGN_RES_S alignRes;	 /* alignment results */
		IMP_S16 faceQA_Res;			 /* faceQA results */

	}FACE_PROP_S;

	typedef struct impOUT_RESULT_Internal_S
	{
		IMP_S32 s32Facenumber;				/* Number of face in current processing frame */
		FACE_PROP_S stFace[MAXNDETECTIONS];	/* face properties */
		IMP_U8 flagShow;

	}OUT_RESULT_S;

	typedef struct impVFD_MODULE_Internal_S
	{

		IMP_HANDLE hFDetectModule;

		OUT_RESULT_S	m_fiOut;			/* final output */
		SAT_BUFF_S   *pruningBuff;			/* mask image integral image buffer */
		IMP_GRAY_IMAGE_S *pruningMask;		/* use to reduce window slice mask */

		IMP_U32 maxHeight;				/* image_height */
		IMP_U32 maxWidth;				/* image_width */
		IMP_U32 fr;						/* number_of_frames */
		VFD_PARA picopara;				/* pico detect parameters */
		IMP_S32 picothreshold;			/* pico detect parameters */

		IMP_U32 u32ProcessMode;
		IMP_U32 u32FDTimes;
		IMP_VFD_TK_PARA_S trackpara;

		IMP_U32 QAlevel;

	}VFD_MODULE_S;
#ifdef __cplusplus
}
#endif

#endif /* IMP_VFD_DETECT_H */