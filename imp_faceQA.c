/***********************************************************************
* FileName: imp_faceQA.c
* Author:  lcy
* Date:  2017/03/31
*
* Description: face quality assessment
*
* modify: code review1
***********************************************************************/

#include "imp_faceQA.h"

#define FACEQA_MAX(a, b) ((a)>(b)?(a):(b))
#define FACEQA_MIN(a, b) ((a)<(b)?(a):(b))
#define THRESHOLD(x,mean) ((x>mean)?(1):(0))
#define BLUR_SIZE 30
#define BLUR_2 ((BLUR_SIZE - 2)*(BLUR_SIZE - 2))
#define BLURS_2 (BLUR_SIZE*BLUR_SIZE)

float evaluate_yaw(IMP_POINT32F_S* f32Shape) 
{
	float vec1_x, vec1_y, vec2_x, vec2_y, vec3_x, vec3_y;
	float vec11_x, vec11_y, vec22_x, vec22_y,vec33_x,vec33_y;

	float eye_left_lenght, eye_right_lenght, vec2_norm;
	float mouth_left_lenght, mouth_right_lenght,vec22_norm;
	float yaw_score;

	vec1_x = f32Shape[NOSE].f32X - f32Shape[EYE1].f32X;
	vec1_y = f32Shape[NOSE].f32Y - f32Shape[EYE1].f32Y;
	vec2_x = f32Shape[EYE2].f32X - f32Shape[EYE1].f32X;
	vec2_y = f32Shape[EYE2].f32Y - f32Shape[EYE1].f32Y;
	vec3_x = f32Shape[NOSE].f32X - f32Shape[EYE2].f32X;
	vec3_y = f32Shape[NOSE].f32Y - f32Shape[EYE2].f32Y;

	vec2_norm =(float) sqrt(vec2_x*vec2_x + vec2_y*vec2_y);
	eye_left_lenght = (vec1_x*vec2_x + vec1_y*vec2_y) / vec2_norm;
	eye_right_lenght = (-vec3_x*vec2_x - vec3_y*vec2_y) / vec2_norm;
	vec11_x = f32Shape[NOSE].f32X - f32Shape[MOUTH1].f32X;
	vec11_y = f32Shape[NOSE].f32Y - f32Shape[MOUTH1].f32Y;
	vec22_x = f32Shape[MOUTH2].f32X - f32Shape[MOUTH1].f32X;
	vec22_y = f32Shape[MOUTH2].f32Y - f32Shape[MOUTH1].f32Y;
	vec33_x = f32Shape[NOSE].f32X - f32Shape[MOUTH2].f32X;
	vec33_y = f32Shape[NOSE].f32Y - f32Shape[MOUTH2].f32Y;


	vec22_norm =(float) sqrt(vec22_x*vec22_x + vec22_y*vec22_y);
	mouth_left_lenght = (vec11_x*vec22_x + vec11_y*vec22_y) / vec22_norm;
	mouth_right_lenght = (-vec33_x*vec2_x - vec33_y*vec22_y) / vec22_norm;

	yaw_score =(float)(0.65* FACEQA_MIN(eye_left_lenght, eye_right_lenght)
		/ FACEQA_MAX(eye_left_lenght, eye_right_lenght)
		+0.35* FACEQA_MIN(mouth_left_lenght, mouth_right_lenght)
		/ FACEQA_MAX(mouth_left_lenght, mouth_right_lenght));
	return yaw_score;
}

float evaluate_roll(IMP_POINT32F_S* f32Shape)
{
	float delta_eye_y, delta_eye_x, theta;
	delta_eye_y = f32Shape[EYE1 ].f32Y - f32Shape[EYE2 ].f32Y;
	delta_eye_x = f32Shape[EYE1 ].f32X - f32Shape[EYE2 ].f32X;
	theta = (float)(fabsf((float)atan(delta_eye_y / delta_eye_x)) * 180 / 3.14159);
	return theta;
}

int evaluate_pitch(IMP_POINT32F_S* f32Shape) 
{
	float eye_y,mouth_y,up;
	eye_y = (f32Shape[EYE1].f32Y + f32Shape[EYE2 ].f32Y)*0.5f;
	mouth_y = (f32Shape[MOUTH1 ].f32Y + f32Shape[MOUTH2 ].f32Y)*0.5f;
	up = (float)(f32Shape[NOSE ].f32Y - eye_y) / (mouth_y - eye_y);

	if (up < 0.23 ) return -1;
	return 0;
}

int IMP_BSD_GrayImgDnSz(IMP_GRAY_IMAGE_S *pstSrc, IMP_GRAY_IMAGE_S*pstDst)
{
	float scl_x, scl_y;
	IMP_S32 i, j, sx, sy;
	IMP_S32 src_w, src_h, dst_w, dst_h;

	IMP_U8 *pu8Row;

	IMP_U8 * pu8Src;
	IMP_U8 * pu8Dst;


	pu8Src = pstSrc->pu8Data;
	pu8Dst = pstDst->pu8Data;

	src_w = pstSrc->s32W;
	src_h = pstSrc->s32H;

	dst_w = pstDst->s32W;
	dst_h = pstDst->s32H;

	scl_x = src_w / (float)dst_w;
	scl_y = src_h / (float)dst_h;

	for (i = 0; i < dst_h; i++)
	{
		sx = FACEQA_MIN((int)(scl_y * i), src_h - 1);
		pu8Row = pu8Src + src_w * sx;
		for (j = 0; j < dst_w; j++)
		{
			sy = FACEQA_MIN((int)(scl_x * j), src_w - 1);
			*pu8Dst = *(pu8Row + sy);
			pu8Dst++;
		}
	}
	return IMP_SUCCESS;
}

float evaluate_blur(IMAGE3_S* image, IMP_RECT_S* roi) 
{
	int i, j, m, w, h, img_w, img_h;
	int res[BLUR_2] = { 0 };
	int index = 0;
	int idx_f = 0;
	int s = 0;
	float mean;
	float v = 0;
	IMP_GRAY_IMAGE_S* face;
	IMP_GRAY_IMAGE_S smallFace;
	img_h = image->s32H;
	img_w = image->s32W;
	if (roi->s16X1 < 0 || roi->s16Y1 < 0 
		|| roi->s16X2 >= img_w || roi->s16Y2 >= img_h) {
		return 0;
	}
	smallFace.s32W = BLUR_SIZE;
	smallFace.s32H = BLUR_SIZE;
	smallFace.pu8Data = (IMP_U8*)malloc(sizeof(IMP_U8)*BLURS_2);

	face = (IMP_GRAY_IMAGE_S*)malloc(sizeof(IMP_GRAY_IMAGE_S));
	w = roi->s16X2 - roi->s16X1;
	h = roi->s16Y2 - roi->s16Y1;
	face->s32W = w;
	face->s32H = h;
	face->pu8Data = (IMP_U8*)malloc(sizeof(IMP_U8)*w*h);

	for (i = 0; i < h; i++)
	{
		for (j = 0; j < w; j++)
		{
			face->pu8Data[i*w + j] = image->pu8D1[(i + roi->s16Y1)*img_w + j + roi->s16X1];
		}
	}
	IMP_BSD_GrayImgDnSz(face, &smallFace);

	free(face->pu8Data);
	free(face);

	for (j = 1; j < BLUR_SIZE - 1; j++)
	{
		for (i = 1; i < BLUR_SIZE - 1; i++)
		{
			index = (i - 1)*(BLUR_SIZE - 2) + j - 1;
			idx_f = i *BLUR_SIZE + j;
			m = smallFace.pu8Data[idx_f - BLUR_SIZE] +
				smallFace.pu8Data[idx_f - 1] +
				smallFace.pu8Data[idx_f + 1] +
				smallFace.pu8Data[idx_f + BLUR_SIZE] -
				smallFace.pu8Data[idx_f] * 4;
			if (m < 0)  res[index] = 0;
			else  res[index] = m;
		}
	}
	free(smallFace.pu8Data);

	for (i = 0; i < BLUR_2; i++) 
	{
		s += res[i];
	}
	mean = (float)s / BLUR_2;


	for (i = 0; i < BLUR_2; i++) 
	{
		v += (res[i] - mean)* (res[i] - mean);
	}

	v = v / (BLUR_2 * BLURS_2);
	return (float)(1. - exp(-1.1*v));
}

/***********************************************************************
* Function: imp_evaluate_faceQA
* Description: face quality assessment
* Parameters:
*	image : IMAGE3_S type pointer refer to a YUV image, as input image
*   roi: IMP_RECT_S type pointer, boundingBox of face detected by Pico
*   res: IMP_Align_RES_S type pointer for alignment results
*   QAlevel: integer type: 0,1,2,3,4,5
*           0 for close faceQA
*           the higner QAlevel, the higner face quality
* Return: an integer score for faceQA:
*         -1 for no face quality assessment
*         0 for bad quality
*         1 for good quality
*
* Example:
*        IMAGE3_S* yuvimage=imread(imagename);
*        IMP_RECT_S roi;
*		  roi.s16X1=10;
*         roi.s16Y1=30;
*		  roi.s16X2=150;
*         roi.s16Y2=170;
*        IMP_Align_RES_S res;
*        res.align_success=0;
*        int QA_level=3;
*        int faceQuality= evaluate_faceQA(yuvimage,&roi,&res,QA_level);
***********************************************************************/
int evaluate_faceQA(IMAGE3_S* image, 
	IMP_RECT_S* roi, 
	IMP_ALIGN_RES_S* res,
	int QA_level) 
{
	float blur;

	if (QA_level == 0) 
	{
		res->align_success = -1;
		return -1;
	}

	blur = evaluate_blur(image, roi);

	if (QA_level == 1) 
	{
		if (blur < 0.45) 
		{
			res->align_success = -1;
			return 0;
		}
		imp_align_pred_shape(image, roi, res);
		if (res->align_success) 
		{
			if (evaluate_roll(res->f32Shape) >= 15)			return 0;
			else if (evaluate_yaw(res->f32Shape) < 0.425)		return 0;
			else if (evaluate_pitch(res->f32Shape))				return 0;
			return 1;
		}
		return 0;
	}

	if (QA_level == 2) 
	{
		if (blur < 0.5) 
		{
			res->align_success = -1;
			return 0;
		}
		imp_align_pred_shape(image, roi, res);
		if (res->align_success) 
		{
			if (evaluate_roll(res->f32Shape) >= 12)			return 0;
			else if (evaluate_yaw(res->f32Shape) < 0.53)		return 0;
			else if (evaluate_pitch(res->f32Shape))				return 0;
			return 1;
		}
		return 0;
	}

	if (QA_level == 3) 
	{
		if (blur < 0.58)
		{
			res->align_success = -1;
			return 0;
		}
		imp_align_pred_shape(image, roi, res);
		if (res->align_success) 
		{
			if (evaluate_roll(res->f32Shape) >= 11)			return 0;
			else if (evaluate_yaw(res->f32Shape) < 0.57)		return 0;
			else if (evaluate_pitch(res->f32Shape))				return 0;
			return 1;
		}
		return 0;
	}

	if (QA_level == 4) 
	{
		if (blur < 0.65) 
		{
			res->align_success = -1;
			return 0;
		}
		imp_align_pred_shape(image, roi, res);
		if (res->align_success) 
		{
			if (evaluate_roll(res->f32Shape) >= 9.5)			return 0;
			else if (evaluate_yaw(res->f32Shape) < 0.59)		return 0;
			else if (evaluate_pitch(res->f32Shape))				return 0;
			return 1;
		}
		return 0;
	}

	if (QA_level == 5) 
	{
		if (blur < 0.7) 
		{
			res->align_success = -1;
			return 0;
		}
		imp_align_pred_shape(image, roi, res);
		if (res->align_success) 
		{
			if (evaluate_roll(res->f32Shape) >= 9)			return 0;
			else if (evaluate_yaw(res->f32Shape) < 0.6)		return 0;
			else if (evaluate_pitch(res->f32Shape))				return 0;
			return 1;
		}
		return 0;
	}

	return -1;
}

