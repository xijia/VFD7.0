#ifndef _IMP_VFD_CASCADE_H_
#define _IMP_VFD_CASCADE_H_

#include <stdint.h>
#include<stdlib.h>
#include<string.h>
#include "imp_vfd_para.h"

#define MAXNDETECTIONS IMP_VFD_MAX_FACE_NUMBER
#define INNERMAXDETECT 512
#ifdef __cplusplus
extern "C"
{
#endif

typedef long long sumtype;

typedef struct impSAT_BUFF_Internal_S
{
	/*image data buffer*/
	int	s32W; 
	int	s32H; 
	int	s32P; 

	int	s32SP;

	sumtype	*u32Sum;

}SAT_BUFF_S;

typedef struct impVFD_Internal_PARA
{
	IMP_S32 faceminsize;	     
	IMP_S32 facemaxsize;		
	float scalefactor;			//how much to expand the window
	float stridefactor;         //how much to move the window between neighboring
	IMP_S32* facethreshold;   
	IMP_S32* facecenterY;   
	IMP_S32* facecenterX;  
	IMP_S32* faceradius;   
	float* rotateangle;              
	IMP_S32 ndetections; 

}VFD_PARA;



IMP_S32 IMP_Pry_findobjects(VFD_PARA* picopara, IMAGE3_S* pstImage, IMP_VFD_PARA_S *m_Para, int ndetections);

IMP_S32 IMP_Color_Pyr_findobjects(VFD_PARA* picopara, IMAGE3_S* pstImage, sumtype* u32Sum, 
														IMP_VFD_PARA_S *m_Para, int ndetections);

IMP_S32 IMP_Color_findobjects(VFD_PARA* picopara, IMAGE3_S* pstImage, sumtype* u32Sum, IMP_VFD_PARA_S *m_Para);

IMP_S32 IMP_Gray_findobjects(VFD_PARA* picopara, IMAGE3_S* pstImage, IMP_VFD_PARA_S *m_Para);

IMP_S32 run_cascade(long long* o, IMP_S32 r, IMP_S32 c, IMP_S32 s, IMAGE3_S* pstImage);

IMP_S32 run_rotated_cascade(long long* o, IMP_S32 r, IMP_S32 c, IMP_S32 s, IMAGE3_S* pstImage, float a);

IMP_S32 cluster_detections(VFD_PARA* picopara, IMP_U16 maxnum);
#ifdef __cplusplus
}
#endif

#endif /* _IMP_VFD_CASCADE_H_ */