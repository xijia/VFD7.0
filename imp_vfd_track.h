/***********************************************************************
* FileName: track.h
* Version:  2.0.0
* Author:	XJ
*
* Date:		2017/01/16
* Update:	2017/04/07
*
* Copyright (C) 2017, IMPower Technologies, all rights reserved.
*
* Description: structures and functions for track function
*
***********************************************************************/

#ifndef _TARGET_H_
#define _TARGET_H_

#include<string.h>
#include<math.h>
#include "imp_vfd_para.h"

#define NUM_BINS 512   /*BIN NUMBERS*/
#define DIST 0.5        /*SHORTEST DISTANCE*/


#ifdef __cplusplus
extern "C"
{
#endif

	typedef struct impVFD_TK_SIZE_S
	{

		IMP_S16 s16W;      /*frame width*/
		IMP_S16 s16H;

	} IMP_VFD_TK_SIZE_S;

	typedef struct impVFD_TK_TARGET_S
	{
		IMP_RECT_S stROI;

		/* para for delete target */
		IMP_S32 s32TkFrame;		         /* preserve for future */
		IMP_S32 s32Cred;                  	 /* for target delete */
		IMP_U8  u8Crsflg;             	 /* target cross flag */

		IMP_U32 u32ID;
		IMP_U8  u8Indx;

		/* show para */
		IMP_U8  u8Dtcflg;             	/* use for align */
		IMP_U8  u8Confd;             /* confirm to track */
		IMP_U8  u8Showflg;                    /* confirm to show */

		IMP_FLOAT  *pf32MsftWei;                 /* meanshift weight*/
		IMP_FLOAT   f32VecTkHist[NUM_BINS];    /* original histogram*/

	} IMP_VFD_TK_TARGET_S;


	typedef struct impVFD_DETECT_RECTS
	{
		IMP_S16 s16X1;
		IMP_S16 s16Y1;
		IMP_S16 s16X2;
		IMP_S16 s16Y2;

		IMP_U8 u8track_flag;
	}IMP_VFD_DETECT_RECTS_S;


	typedef struct impVFD_TK_PARA_S
	{

		IMP_VFD_TK_TARGET_S stVecTarget[MAXNDETECTIONS];
		IMP_VFD_TK_SIZE_S   stWinSz;

		IMP_S32   s32VecDtc[MAXNDETECTIONS * 4 + 1];
		IMP_U8 	  u8VecRoiIndx[MAXNDETECTIONS * 2 + 4];

		/* config */
		IMP_U16 u16IteraNum;
		IMP_U8  u8Confds;
		IMP_U8  u8Sense;

		IMP_U16   * pu16RgbTmp;                 /* temp rgb calculate */
		IMP_FLOAT   f32VecWeiTmp[NUM_BINS];    /* temp weight calculate */
		IMP_FLOAT   f32VecHistTmp[NUM_BINS];      /* temp histogram */

		/* tracking fragment variables*/
		IMP_U8  * pu8RgbBuf;                /* rgb data */
		IMP_U8    u8DtcIndx;				/* detection index */
		IMP_U8    u8TkNum;
		IMP_U8    u8TkIndx;
		IMP_U32   u32TkID;

		IMP_U8 u8DetectNum;
		IMP_VFD_DETECT_RECTS_S pstDetect[MAXNDETECTIONS];


	}IMP_VFD_TK_PARA_S;

	/***********************************************************************
	* Function:     IMP_VFD_TK_YuvtRgb
	* Description: 	transfer yuv data to rgb data
	* Input:
	*	   IMAGE3_S 	*frame 		source yuv image data
	*	   IMP_U8 		*rgb	    rgb saver address
	* Output:
	*	   IMP_U8		*rgb 		rgb data
	*
	* Return:
	*       IMP_SUCCESS   success
	*
	***********************************************************************/
	IMP_U32 IMP_VFD_TK_YuvtRgb(IMAGE3_S *frame, IMP_U8 *rgb);


	/***********************************************************************
	* Function:    IMP_VFD_TK_create
	* Description: tracking parameters memory allocation
	* Input:
	*	 * pstTKPara	 tracking parameters
	*      height		 height of source image
	* 	   width		 width of source image
	* Output:
	*	   pstTKPara	 initial tracking parameters
	* Return:
	*      status
	***********************************************************************/
	IMP_S32 IMP_VFD_TK_create(IMP_VFD_TK_PARA_S *pstTKPara,
		IMP_S32  height,
		IMP_S32  width);


	/***********************************************************************
	* Function:    IMP_VFD_TK_color
	* Description: color tracking main program
	* Input:
	*		VFD_MODULE_S	 *m_hPara,
	*		IMAGE3_S		 *pstImage
	*
	* Output:
	*		IMP_SUCCESS		success
	* Return:
	*		 free status
	***********************************************************************/
	IMP_U32 IMP_VFD_TK_color(IMP_HANDLE hModule, IMAGE3_S *pstImage);

	IMP_U32 IMP_VFD_TK_gray(IMP_HANDLE hModule, IMAGE3_S *pstImage);

	/***********************************************************************
	* Function:    IMP_VFD_TK_release
	* Description: tracking parameters memory free
	* Input:
	*		*pstTKPara	 tracking parameters
	*
	* Output:
	*		*pstTKPara	 free malloc tracking parameters
	* Return:
	*		 free status
	***********************************************************************/
	IMP_S32 IMP_VFD_TK_release(IMP_VFD_TK_PARA_S *pstTKPara);


#ifdef __cplusplus
}
#endif


#endif//#ifndef TARGET_H