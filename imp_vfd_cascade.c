

#include "imp_vfd_cascade.h"
#define MAX(a, b) ((a)>(b)?(a):(b))
#define MIN(a, b) ((a)<(b)?(a):(b))

extern int luts[];
extern char tcodes[];

static int thresholds[] = {



	-80506, -133700000, -121506, -133700000, -133700000, -155006, -133700000, -133700000, -133700000, -133700000,
	-133700000, -197506, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000,
	-133700000, -230007, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000,
	-133700000, -235507, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000,
	-133700000, -133700000, -235507, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000,
	-133700000, -133700000, -133700000, -133700000, -133700000, -238007, -133700000, -133700000, -133700000, -133700000,
	-133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -236007,
	-133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000,
	-133700000, -133700000, -133700000, -133700000, -133700000, -236507, -133700000, -133700000, -133700000, -133700000,
	-133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000,
	-133700000, -133700000, -133700000, -235007, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000,
	-133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000,
	-133700000, -133700000, -133700000, -234007, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000,
	-133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000,
	-133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -233507, -133700000, -133700000, -133700000,
	-133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000,
	-133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -232507,
	-133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000,
	-133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000,
	-133700000, -133700000, -133700000, -230507, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000,
	-133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000,
	-133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -229507,
	-133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000,
	-133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000,
	-133700000, -133700000, -133700000, -133700000, -229007, -133700000, -133700000, -133700000, -133700000, -133700000,
	-133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000,
	-133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000,
	-133700000, -223007, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000,
	-133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000,
	-133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -219506, -133700000,
	-133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000,
	-133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000, -133700000,
	-133700000, -133700000, -133700000, -133700000, -215006 };

static int qcostable[32 + 1] = { 256, 251, 236, 212, 181, 142, 97, 49, 0, -49, -97, -142, -181, -212, -236, -251, -256, -251, -236, -212, -181, -142, -97, -49, 0, 49, 97, 142, 181, 212, 236, 251, 256 };
static int qsintable[32 + 1] = { 0, 49, 97, 142, 182, 212, 236, 251, 256, 251, 236, 212, 181, 142, 97, 49, 0, -49, -97, -142, -181, -212, -236, -251, -256, -251, -236, -212, -181, -142, -97, -49, 0 };

IMP_S32 run_cascade(long long* o, IMP_S32 r, IMP_S32 c, IMP_S32 s, IMAGE3_S* pstImage)
{
	int i, j, idx;
	uint8_t* pixels;
	int tdepth, ntrees;
	long long thr = 0;
	pixels = (uint8_t*)pstImage->pu8D1;
	int acc_a, tmp_b, tmp_c, tmp_d, tmp_e;

	tdepth = 6;
	ntrees = 325;
	int8_t x1, x2;
	int8_t y1, y2;

	tmp_e = 1 << tdepth;
	tmp_c = tmp_e - 1;

	r = r << 8;
	c = c << 8;

	if ((r + (s << 7)) / 256 >= pstImage->s32H || (r - (s << 7)) / 256<0 || (c + (s << 7)) / 256 >= pstImage->s32W || (c - (s << 7)) / 256<0)
		return -1;

	*o = 0;
	acc_a = -tmp_c;
	for (i = 0; i<ntrees; ++i)
	{
		idx = 1;
		acc_a += tmp_c;
		tmp_d = acc_a << 2;
		for (j = 0; j<tdepth; ++j)
		{
			tmp_b = ((idx - 1) << 2) + tmp_d;
			y1 = tcodes[tmp_b];
			x1 = tcodes[tmp_b + 1];
			y2 = tcodes[tmp_b + 2];
			x2 = tcodes[tmp_b + 3];
			idx = (idx << 1) + (pixels[(r + y1*s) / 256 * pstImage->s32W + (c + x1*s) / 256] <= pixels[(r + y2*s) / 256 * pstImage->s32W + (c + x2*s) / 256]);

		}
		*o = *o + luts[idx - tmp_e + tmp_e*i];

		if (*o <= thresholds[i])
		{
			return -1;
		}

	}

	*o = *o - thresholds[i - 1];
	return +1;
}


IMP_S32 run_rotated_cascade(long long* o, IMP_S32 r, IMP_S32 c, IMP_S32 s, IMAGE3_S* pstImage, float a)
{
	//
	int i, j, idx;
	uint8_t* pixels;
	int tdepth, ntrees;
	long long thr = 0;
	int acc_a, tmp_b, tmp_c, tmp_d, tmp_e;
	long long qsin = s*qsintable[(int)(32 * a)];
	long long qcos = s*qcostable[(int)(32 * a)];
	int8_t x1, x2;
	int8_t y1, y2;
	int r1, c1, r2, c2;

	pixels = (uint8_t*)pstImage->pu8D1;

	tdepth = 6;
	ntrees = 325;

	tmp_e = 1 << tdepth;
	tmp_c = tmp_e - 1;

	r = r << 16;
	c = c << 16;

	if ((r + 46341 * s) / 65536 >= pstImage->s32H || (r - 46341 * s) / 65536<0 || (c + 46341 * s) / 65536 >= pstImage->s32W || (c - 46341 * s) / 65536<0)
		return -1;

	*o = 0;
	acc_a = -tmp_c;



	for (i = 0; i<ntrees; ++i)
	{
		idx = 1;
		acc_a += tmp_c;
		tmp_d = acc_a << 2;
		for (j = 0; j<tdepth; ++j)
		{
			tmp_b = ((idx - 1) << 2) + tmp_d;
			y1 = tcodes[tmp_b];
			x1 = tcodes[tmp_b + 1];
			y2 = tcodes[tmp_b + 2];
			x2 = tcodes[tmp_b + 3];

			r1 = (r + qcos*y1 - qsin*x1) / 65536;
			c1 = (c + qsin*y1 + qcos*x1) / 65536;
			r2 = (r + qcos*y2 - qsin*x2) / 65536;
			c2 = (c + qsin*y2 + qcos*x2) / 65536;

			idx = (idx << 1) + (pixels[r1*pstImage->s32W + c1] <= pixels[r2*pstImage->s32W + c2]);

		}

		*o = *o + luts[idx - tmp_e + tmp_e*i];

		if (*o <= thresholds[i])
		{
			return -1;
		}
	}

	*o = *o - thresholds[i - 1];

	return +1;
}

IMP_S32 IMP_Pry_findobjects(VFD_PARA* picopara, IMAGE3_S* pstImage, IMP_VFD_PARA_S *m_Para, int ndetections)
{
	float s;
	s = (float)picopara->faceminsize;
	int skin_area = 0;
	int winArea = 0;
	int i = 0;
	int s32H = pstImage->s32H;
	int s32W = pstImage->s32W;
	int Vertex1, Vertex2, Vertey1, Vertey2;
	float r, c, dr, dc;

	while (s <= picopara->facemaxsize)
	{
		dr = MAX(picopara->stridefactor*s, 2.0f);
		winArea = (int)(s*s);
		dc = dr;

		if (1 == m_Para->stROI.s32Enable)
		{
			Vertex1 = MAX(m_Para->stROI.stPolygon.astVertex[0].s16X, 0);
			Vertey1 = MAX(m_Para->stROI.stPolygon.astVertex[0].s16Y, 0);
			Vertex2 = MAX(s32W - m_Para->stROI.stPolygon.astVertex[2].s16X, 0);
			Vertey2 = MAX(s32H - m_Para->stROI.stPolygon.astVertex[2].s16Y, 0);
		}
		else
		{
			Vertex1 = 0;
			Vertey1 = 0;
			Vertex2 = 0;
			Vertey2 = 0;
		}

		for (r = s / 2 + 1 + Vertey1; r <= s32H - s / 2 - 1 - Vertey2; r += dr)
		{
			for (c = s / 2 + 1 + Vertex1; c <= s32W - s / 2 - 1 - Vertex2; c += dc)
			{
				int q;
				long long tempq;
				int t = 0;
				t = run_cascade(&tempq, (IMP_S32)r, (IMP_S32)c, (IMP_S32)s, pstImage);
				if (1 == t)
				{
					if (ndetections < INNERMAXDETECT)
					{
						q = (int)(tempq / 100000);
						picopara->facethreshold[ndetections] = q;
						picopara->facecenterY[ndetections] = (IMP_S32)r;
						picopara->facecenterX[ndetections] = (IMP_S32)c;
						picopara->faceradius[ndetections] = (IMP_S32)s;
						picopara->rotateangle[ndetections] = 0;
						++ndetections;
					}
				}
				if (1 == m_Para->u32Enablerotate)
				{
					for (i = 1; i <= (int)MIN(m_Para->u32RotateLevel, 4); i++)
					{
						if (0.125*i<0.50)
						{
							t = run_rotated_cascade(&tempq, (IMP_S32)r, (IMP_S32)c, (IMP_S32)s, pstImage, (float)0.125*i);
							if (1 == t)
							{
								if (ndetections < INNERMAXDETECT)
								{
									q = (int)(tempq / 100000);
									picopara->facethreshold[ndetections] = q;
									picopara->facecenterY[ndetections] = (IMP_S32)r;
									picopara->facecenterX[ndetections] = (IMP_S32)c;
									picopara->faceradius[ndetections] = (IMP_S32)s;
									picopara->rotateangle[ndetections] = (float)0.125*i;
									++ndetections;
								}
							}
							t = run_rotated_cascade(&tempq, (IMP_S32)r, (IMP_S32)c, (IMP_S32)s, pstImage, (float)(1.0 - 0.125*i));
							if (1 == t)
							{
								if (ndetections < INNERMAXDETECT)
								{
									q = (int)(tempq / 100000);
									picopara->facethreshold[ndetections] = q;
									picopara->facecenterY[ndetections] = (IMP_S32)r;
									picopara->facecenterX[ndetections] = (IMP_S32)c;
									picopara->faceradius[ndetections] = (IMP_S32)s;
									picopara->rotateangle[ndetections] = (float)-0.125*i;
									++ndetections;
								}
							}
						}
						else
						{
							t = run_rotated_cascade(&tempq, (IMP_S32)r, (IMP_S32)c, (IMP_S32)s, pstImage, 0.5);
							if (1 == t)
							{
								if (ndetections < INNERMAXDETECT)
								{
									q = (int)(tempq / 100000);
									picopara->facethreshold[ndetections] = q;
									picopara->facecenterY[ndetections] = (IMP_S32)r;
									picopara->facecenterX[ndetections] = (IMP_S32)c;
									picopara->faceradius[ndetections] = (IMP_S32)s;
									picopara->rotateangle[ndetections] = 0.5;
									++ndetections;
								}
							}
						}
					}
				}
			}
		}
		s = picopara->scalefactor*s;
	}
	picopara->ndetections = ndetections;
	return 0;
}

IMP_S32 IMP_Color_Pyr_findobjects(VFD_PARA* picopara, IMAGE3_S* pstImage, sumtype* u32Sum, IMP_VFD_PARA_S *m_Para, int ndetections)
{
	float s;
	s = (float)picopara->faceminsize;
	int skin_area = 0, winArea = 0;
	int i = 0;
	int s32H = pstImage->s32H;
	int s32W = pstImage->s32W;
	IMP_RECT_S winRect;
	int Vertex1, Vertex2, Vertey1, Vertey2;
	float r, c, dr, dc;
	while (s <= picopara->facemaxsize)
	{
		dr = MAX(picopara->stridefactor*s, 2.0f);
		winArea = (int)(s*s);
		dc = dr;

		if (1 == m_Para->stROI.s32Enable)
		{
			Vertex1 = MAX(m_Para->stROI.stPolygon.astVertex[0].s16X, 0);
			Vertey1 = MAX(m_Para->stROI.stPolygon.astVertex[0].s16Y, 0);
			Vertex2 = MAX(s32W - m_Para->stROI.stPolygon.astVertex[2].s16X, 0);
			Vertey2 = MAX(s32H - m_Para->stROI.stPolygon.astVertex[2].s16Y, 0);
		}
		else
		{
			Vertex1 = 0;
			Vertey1 = 0;
			Vertex2 = 0;
			Vertey2 = 0;
		}

		for (r = s / 2 + 1 + Vertey1; r <= s32H - s / 2 - 1 - Vertey2; r += dr)
		{
			for (c = s / 2 + 1 + Vertex1; c <= s32W - s / 2 - 1 - Vertex2; c += dc)
			{
				winRect.s16X1 = (IMP_S16)(c - s / 2);
				winRect.s16X2 = (IMP_S16)(c + s / 2);
				winRect.s16Y1 = (IMP_S16)(r - s / 2);
				winRect.s16Y2 = (IMP_S16)(r + s / 2);

				skin_area = (int)(u32Sum[winRect.s16X2 + 1 + winRect.s16Y2*(s32W + 1)] - u32Sum[winRect.s16X1 + 1 + winRect.s16Y2*(s32W + 1)]
					- u32Sum[winRect.s16X2 + 1 + winRect.s16Y1*(s32W + 1)] + u32Sum[winRect.s16X1 + 1 + winRect.s16Y1*(s32W + 1)]);

				if ((4 * skin_area) <  winArea || skin_area == winArea)
				{
					continue;
				}
				int q;
				long long tempq;
				int t = 0;
				t = run_cascade(&tempq, (IMP_S32)r, (IMP_S32)c, (IMP_S32)s, pstImage);
				//t = run_rotated_cascade(&tempq, r, c, s, pstImage,0.08);
				if (1 == t)
				{
					if (ndetections < INNERMAXDETECT)
					{
						q = (int)(tempq / 100000);
						picopara->facethreshold[ndetections] = q;
						picopara->facecenterY[ndetections] = (IMP_S32)r;
						picopara->facecenterX[ndetections] = (IMP_S32)c;
						picopara->faceradius[ndetections] = (IMP_S32)s;
						picopara->rotateangle[ndetections] = 0;
						++ndetections;
					}
				}
				if (1 == m_Para->u32Enablerotate)
				{
					for (i = 1; i <= (int)MIN(m_Para->u32RotateLevel, 4); i++)
					{
						if (0.125*i<0.50)
						{
							t = run_rotated_cascade(&tempq, (IMP_S32)r, (IMP_S32)c, (IMP_S32)s, pstImage, (float)0.125*i);
							if (1 == t)
							{
								if (ndetections < INNERMAXDETECT)
								{
									q = (int)(tempq / 100000);
									picopara->facethreshold[ndetections] = q;
									picopara->facecenterY[ndetections] = (IMP_S32)r;
									picopara->facecenterX[ndetections] = (IMP_S32)c;
									picopara->faceradius[ndetections] = (IMP_S32)s;
									picopara->rotateangle[ndetections] = (float)0.125*i;
									++ndetections;
								}
							}
							t = run_rotated_cascade(&tempq, (IMP_S32)r, (IMP_S32)c, (IMP_S32)s, pstImage, (float)(1.0 - 0.125*i));
							if (1 == t)
							{
								if (ndetections < INNERMAXDETECT)
								{
									q = (int)(tempq / 100000);
									picopara->facethreshold[ndetections] = q;
									picopara->facecenterY[ndetections] = (IMP_S32)r;
									picopara->facecenterX[ndetections] = (IMP_S32)c;
									picopara->faceradius[ndetections] = (IMP_S32)s;
									picopara->rotateangle[ndetections] = (float)-0.125*i;
									++ndetections;
								}
							}
						}
						else
						{
							t = run_rotated_cascade(&tempq, (IMP_S32)r, (IMP_S32)c, (IMP_S32)s, pstImage, 0.5);
							if (1 == t)
							{
								if (ndetections < INNERMAXDETECT)
								{
									q = (int)(tempq / 100000);
									picopara->facethreshold[ndetections] = q;
									picopara->facecenterY[ndetections] = (IMP_S32)r;
									picopara->facecenterX[ndetections] = (IMP_S32)c;
									picopara->faceradius[ndetections] = (IMP_S32)s;
									picopara->rotateangle[ndetections] = 0.5;
									++ndetections;
								}
							}
						}
					}
				}
			}
		}
		s = picopara->scalefactor*s;

	}
	picopara->ndetections = ndetections;
	return 0;
}

IMP_S32 IMP_Color_findobjects(VFD_PARA* picopara, IMAGE3_S* pstImage, sumtype* u32Sum, IMP_VFD_PARA_S *m_Para)
{
	float s;
	int ndetections = 0;
	s = (float)picopara->faceminsize;
	int skin_area = 0, winArea = 0;
	int i = 0;
	int s32H = pstImage->s32H;
	int s32W = pstImage->s32W;
	IMP_RECT_S winRect;
	int Vertex1, Vertex2, Vertey1, Vertey2;
	float r, c, dr, dc;
	while (s <= picopara->facemaxsize)
	{
		dr = MAX(picopara->stridefactor*s, 2.0f);
		winArea = (int)(s*s);
		dc = dr;

		if (1 == m_Para->stROI.s32Enable)
		{
			Vertex1 = MAX(m_Para->stROI.stPolygon.astVertex[0].s16X, 0);
			Vertey1 = MAX(m_Para->stROI.stPolygon.astVertex[0].s16Y, 0);
			Vertex2 = MAX(s32W - m_Para->stROI.stPolygon.astVertex[2].s16X, 0);
			Vertey2 = MAX(s32H - m_Para->stROI.stPolygon.astVertex[2].s16Y, 0);
		}
		else
		{
			Vertex1 = 0;
			Vertey1 = 0;
			Vertex2 = 0;
			Vertey2 = 0;
		}

		for (r = s / 2 + 1 + Vertey1; r <= s32H - s / 2 - 1 - Vertey2; r += dr)
		{
			for (c = s / 2 + 1 + Vertex1; c <= s32W - s / 2 - 1 - Vertex2; c += dc)
			{
				winRect.s16X1 = (IMP_S16)(c - s / 2);
				winRect.s16X2 = (IMP_S16)(c + s / 2);
				winRect.s16Y1 = (IMP_S16)(r - s / 2);
				winRect.s16Y2 = (IMP_S16)(r + s / 2);

				skin_area = (int)(u32Sum[winRect.s16X2 + 1 + winRect.s16Y2*(s32W + 1)] - u32Sum[winRect.s16X1 + 1 + winRect.s16Y2*(s32W + 1)]
					- u32Sum[winRect.s16X2 + 1 + winRect.s16Y1*(s32W + 1)] + u32Sum[winRect.s16X1 + 1 + winRect.s16Y1*(s32W + 1)]);

				if ((4 * skin_area) <  winArea || skin_area == winArea)
				{
					continue;
				}
				int q;
				long long tempq;
				int t = 0;
				t = run_cascade(&tempq, (IMP_S32)r, (IMP_S32)c, (IMP_S32)s, pstImage);
				//t = run_rotated_cascade(&tempq, r, c, s, pstImage,0.08);
				if (1 == t)
				{
					if (ndetections < INNERMAXDETECT)
					{
						q = (int)(tempq / 100000);
						picopara->facethreshold[ndetections] = q;
						picopara->facecenterY[ndetections] = (IMP_S32)r;
						picopara->facecenterX[ndetections] = (IMP_S32)c;
						picopara->faceradius[ndetections] = (IMP_S32)s;
						picopara->rotateangle[ndetections] = 0;
						++ndetections;
					}
				}
				if (1 == m_Para->u32Enablerotate)
				{
					for (i = 1; i <= (int)MIN(m_Para->u32RotateLevel, 4); i++)
					{
						if (0.125*i<0.50)
						{
							t = run_rotated_cascade(&tempq, (IMP_S32)r, (IMP_S32)c, (IMP_S32)s, pstImage, (float)0.125*i);
							if (1 == t)
							{
								if (ndetections < INNERMAXDETECT)
								{
									q = (int)(tempq / 100000);
									picopara->facethreshold[ndetections] = q;
									picopara->facecenterY[ndetections] = (IMP_S32)r;
									picopara->facecenterX[ndetections] = (IMP_S32)c;
									picopara->faceradius[ndetections] = (IMP_S32)s;
									picopara->rotateangle[ndetections] = (float) 0.125*i;
									++ndetections;
								}
							}
							t = run_rotated_cascade(&tempq, (IMP_S32)r, (IMP_S32)c, (IMP_S32)s, pstImage, (float)(1.0 - 0.125*i));
							if (1 == t)
							{
								if (ndetections < INNERMAXDETECT)
								{
									q = (int)(tempq / 100000);
									picopara->facethreshold[ndetections] = q;
									picopara->facecenterY[ndetections] = (IMP_S32)r;
									picopara->facecenterX[ndetections] = (IMP_S32)c;
									picopara->faceradius[ndetections] = (IMP_S32)s;
									picopara->rotateangle[ndetections] = (float)-0.125*i;
									++ndetections;
								}
							}
						}
						else
						{
							t = run_rotated_cascade(&tempq, (IMP_S32)r, (IMP_S32)c, (IMP_S32)s, pstImage, 0.5);
							if (1 == t)
							{
								if (ndetections < INNERMAXDETECT)
								{
									q = (int)(tempq / 100000);
									picopara->facethreshold[ndetections] = q;
									picopara->facecenterY[ndetections] = (IMP_S32)r;
									picopara->facecenterX[ndetections] = (IMP_S32)c;
									picopara->faceradius[ndetections] = (IMP_S32)s;
									picopara->rotateangle[ndetections] = 0.5;
									++ndetections;
								}
							}
						}
					}
				}
			}
		}
		s = picopara->scalefactor*s;

	}
	picopara->ndetections = ndetections;
	return 0;
}
IMP_S32 IMP_Gray_findobjects(VFD_PARA* picopara, IMAGE3_S* pstImage, IMP_VFD_PARA_S *m_Para)
{
	float s;
	int ndetections = 0;
	s = (float)picopara->faceminsize;
	int skin_area = 0;
	int winArea = 0;
	int i = 0;
	int s32H = pstImage->s32H;
	int s32W = pstImage->s32W;
	int Vertex1, Vertex2, Vertey1, Vertey2;
	float r, c, dr, dc;

	while (s <= picopara->facemaxsize)
	{
		dr = MAX(picopara->stridefactor*s, 2.0f);
		winArea = (int)(s*s);
		dc = dr;

		if (1 == m_Para->stROI.s32Enable)
		{
			Vertex1 = MAX(m_Para->stROI.stPolygon.astVertex[0].s16X, 0);
			Vertey1 = MAX(m_Para->stROI.stPolygon.astVertex[0].s16Y, 0);
			Vertex2 = MAX(s32W - m_Para->stROI.stPolygon.astVertex[2].s16X, 0);
			Vertey2 = MAX(s32H - m_Para->stROI.stPolygon.astVertex[2].s16Y, 0);
		}
		else
		{
			Vertex1 = 0;
			Vertey1 = 0;
			Vertex2 = 0;
			Vertey2 = 0;
		}

		for (r = s / 2 + 1 + Vertey1; r <= s32H - s / 2 - 1 - Vertey2; r += dr)
		{
			for (c = s / 2 + 1 + Vertex1; c <= s32W - s / 2 - 1 - Vertex2; c += dc)
			{
				int q;
				long long tempq;
				int t = 0;
				t = run_cascade(&tempq, (IMP_S32)r, (IMP_S32)c, (IMP_S32)s, pstImage);
				if (1 == t)
				{
					if (ndetections < INNERMAXDETECT)
					{
						q = (int)(tempq / 100000);
						picopara->facethreshold[ndetections] = q;
						picopara->facecenterY[ndetections] = (IMP_S32)r;
						picopara->facecenterX[ndetections] = (IMP_S32)c;
						picopara->faceradius[ndetections] = (IMP_S32)s;
						picopara->rotateangle[ndetections] = 0;
						++ndetections;
					}
				}
				if (1 == m_Para->u32Enablerotate)
				{
					for (i = 1; i <= (int)MIN(m_Para->u32RotateLevel, 4); i++)
					{
						if (0.125*i<0.50)
						{
							t = run_rotated_cascade(&tempq, (IMP_S32)r, (IMP_S32)c, (IMP_S32)s, pstImage, (float)0.125*i);
							if (1 == t)
							{
								if (ndetections < INNERMAXDETECT)
								{
									q = (int)(tempq / 100000);
									picopara->facethreshold[ndetections] = q;
									picopara->facecenterY[ndetections] = (IMP_S32)r;
									picopara->facecenterX[ndetections] = (IMP_S32)c;
									picopara->faceradius[ndetections] = (IMP_S32)s;
									picopara->rotateangle[ndetections] = (float) 0.125*i;
									++ndetections;
								}
							}
							t = run_rotated_cascade(&tempq, (IMP_S32)r, (IMP_S32)c, (IMP_S32)s, pstImage, (float)(1.0 - 0.125*i));
							if (1 == t)
							{
								if (ndetections < INNERMAXDETECT)
								{
									q = (int)(tempq / 100000);
									picopara->facethreshold[ndetections] = q;
									picopara->facecenterY[ndetections] = (IMP_S32)r;
									picopara->facecenterX[ndetections] = (IMP_S32)c;
									picopara->faceradius[ndetections] = (IMP_S32)s;
									picopara->rotateangle[ndetections] = (float)-0.125*i;
									++ndetections;
								}
							}
						}
						else
						{
							t = run_rotated_cascade(&tempq, (IMP_S32)r, (IMP_S32)c, (IMP_S32)s, pstImage, 0.5);
							if (1 == t)
							{
								if (ndetections < INNERMAXDETECT)
								{
									q = (int)(tempq / 100000);
									picopara->facethreshold[ndetections] = q;
									picopara->facecenterY[ndetections] = (IMP_S32)r;
									picopara->facecenterX[ndetections] = (IMP_S32)c;
									picopara->faceradius[ndetections] = (IMP_S32)s;
									picopara->rotateangle[ndetections] = 0.5;
									++ndetections;
								}
							}
						}
					}
				}
			}
		}
		s = picopara->scalefactor*s;
	}
	picopara->ndetections = ndetections;
	return 0;
}


float get_overlap(IMP_S32 r1, IMP_S32 c1, IMP_S32 s1, IMP_S32 r2, IMP_S32 c2, IMP_S32 s2)
{
	IMP_S32 overr, overc;

	overr = MAX(0, MIN(r1 + s1 / 2, r2 + s2 / 2) - MAX(r1 - s1 / 2, r2 - s2 / 2));
	overc = MAX(0, MIN(c1 + s1 / 2, c2 + s2 / 2) - MAX(c1 - s1 / 2, c2 - s2 / 2));

	return (float)(1.0*overr*overc) / (s1*s1 + s2*s2 - overr*overc);
}

void ccdfs(IMP_S32 a[], IMP_S32 i, IMP_S32* rs, IMP_S32* cs, IMP_S32* ss, IMP_S32 n)
{
	int j;

	//
	for (j = 0; j<n; ++j)
	{
		if (a[j] == 0 && get_overlap(rs[i], cs[i], ss[i], rs[j], cs[j], ss[j])>0.3f)
		{
			a[j] = a[i];
			ccdfs(a, j, rs, cs, ss, n);
		}
	}
}

int find_connected_components(IMP_S32 a[], IMP_S32* rs, IMP_S32* cs, IMP_S32* ss, IMP_S32 n)
{
	int i, ncc, cc;

	//
	if (!n)
		return 0;

	//
	for (i = 0; i<n; ++i)
		a[i] = 0;

	//
	ncc = 0;
	cc = 1;

	for (i = 0; i<n; ++i)
		if (a[i] == 0)
		{
			//
			a[i] = cc;

			//
			ccdfs(a, i, rs, cs, ss, n);

			//
			++ncc;
			++cc;
		}

	//
	return ncc;
}


IMP_S32 cluster_detections(VFD_PARA* picopara, IMP_U16 maxnum)
{
	int idx, ncc, cc;
	int a[4096];

	//
	ncc = find_connected_components(a, picopara->facecenterY, picopara->facecenterX, picopara->faceradius, picopara->ndetections);

	if (!ncc)
		return 0;

	//
	idx = 0;

	for (cc = 1; cc <= ncc; ++cc)
	{
		int i, k;

		float sumqs = 0.0f, sumrs = 0.0f, sumcs = 0.0f, sumss = 0.0f;
		k = 0;

		for (i = 0; i<picopara->ndetections; ++i)
		{
			if (a[i] == cc)
			{
				sumqs += picopara->facethreshold[i];
				sumrs += picopara->facecenterY[i];
				sumcs += picopara->facecenterX[i];
				sumss += picopara->faceradius[i];
				++k;
			}
		}
		//
		picopara->facethreshold[idx] = (IMP_S32)sumqs; // accumulated confidence measure
		picopara->facecenterY[idx] = (IMP_S32)(sumrs / k);
		picopara->facecenterX[idx] = (IMP_S32)(sumcs / k);
		picopara->faceradius[idx] = (IMP_S32)(sumss / k);
		++idx;
		if (idx >= MIN(MAXNDETECTIONS, maxnum))
		{
			break;
		}

	}
	if (idx > MIN(MAXNDETECTIONS, maxnum))
	{
		idx = MIN(MAXNDETECTIONS, maxnum);
	}

	picopara->ndetections = idx;

	//
	return 0;
}


