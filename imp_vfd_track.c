/***********************************************************************
* FileName: track.c
* Version:  2.0.0
* Author:  xj
* Copyright (C) 2016, IMPower Technologies, all rights reserved.
*
* Description: structures and functions for track people face
*
* Date:	170407
***********************************************************************/


#include"imp_vfd_detect.h"

#define MAX_2(a, b) ((a)>(b)?(a):(b))
#define MIN_2(a, b) ((a)<(b)?(a):(b))

/***********************************************************************
* Function:    IMP_VFD_TK_create
* Description: tracking parameters memory allocation
* Input:
*	   *pstTKPara	 tracking parameters
*      	height		 height of source image
* 	   	width		 width of source image
* Output:
*	   pstTKPara	 initial tracking parameters
* Return:
*      status
***********************************************************************/
IMP_S32 IMP_VFD_TK_create(IMP_VFD_TK_PARA_S *pstTKPara,
	IMP_S32 height,
	IMP_S32 width)
{
	IMP_U8 i,j;
	for (i = 0; i < MAXNDETECTIONS; i++)
	{
		pstTKPara->stVecTarget[i].u8Indx = i;
		pstTKPara->stVecTarget[i].s32Cred = 0;
		pstTKPara->stVecTarget[i].u32ID = 0;
		pstTKPara->stVecTarget[i].s32TkFrame = 0;

		pstTKPara->stVecTarget[i].stROI.s16X1 = 1;
		pstTKPara->stVecTarget[i].stROI.s16Y1 = 1;
		pstTKPara->stVecTarget[i].stROI.s16X2 = 1;
		pstTKPara->stVecTarget[i].stROI.s16Y2 = 1;

		pstTKPara->stVecTarget[i].u8Dtcflg = 0;
		pstTKPara->stVecTarget[i].u8Confd = 0;
		pstTKPara->stVecTarget[i].u8Showflg = 0;

		pstTKPara->stVecTarget[i].pf32MsftWei = (IMP_FLOAT*)malloc(sizeof(IMP_FLOAT)* width * height);
		if (pstTKPara->stVecTarget[i].pf32MsftWei == NULL)
		{
			for (j = 0; j < i-1; j++)
			{
				free(pstTKPara->stVecTarget[i].pf32MsftWei);
			}
			return IMP_NULL;
		}

		memset(pstTKPara->stVecTarget[i].pf32MsftWei, 0, sizeof(IMP_FLOAT)* width * height);
		memset(pstTKPara->stVecTarget[i].f32VecTkHist, 0, sizeof(IMP_FLOAT)* NUM_BINS);
	}

	pstTKPara->stWinSz.s16H = height;
	pstTKPara->stWinSz.s16W = width;
	pstTKPara->u8TkNum = 0;
	pstTKPara->u8DtcIndx = 0;
	pstTKPara->u8TkIndx = 0;
	pstTKPara->u32TkID = 0;

	pstTKPara->pu16RgbTmp = (IMP_U16*)malloc(sizeof(IMP_U16)* width * height);
	if (pstTKPara->pu16RgbTmp == NULL)
	{
		for (j = 0; j < MAXNDETECTIONS; j++)
		{
			free(pstTKPara->stVecTarget[i].pf32MsftWei);
		}
		return IMP_NULL;
	}
	memset(pstTKPara->pu16RgbTmp, 0, sizeof(IMP_U16) * width * height);
	memset(pstTKPara->s32VecDtc, 0, sizeof(IMP_S32) * (MAXNDETECTIONS * 4 + 1));
	memset(pstTKPara->u8VecRoiIndx, 0, sizeof(IMP_U8) * (MAXNDETECTIONS * 2 + 4));
	memset(pstTKPara->f32VecWeiTmp, 0, sizeof(IMP_FLOAT)* NUM_BINS);
	memset(pstTKPara->f32VecHistTmp, 0, sizeof(IMP_FLOAT)*NUM_BINS);
	
	pstTKPara->pu8RgbBuf = (IMP_U8*)malloc(sizeof(IMP_U8) * width * height * 3);
	if (pstTKPara->pu8RgbBuf == NULL)
	{
		
		for (j = 0; j < MAXNDETECTIONS; j++)
		{
			free(pstTKPara->stVecTarget[i].pf32MsftWei);
		}
		free(pstTKPara->pu16RgbTmp);
		return IMP_NULL;
	}

	return 78;
}

/***********************************************************************
* Function:      IMP_VFD_TK_YuvtRgb
* Description: 	 transfer yuv data to rgb data
* Input:
*	   *frame 		source yuv image address
*	   *rgb	    	rgb saver address
*
* Output:
*	   *rgb 		rgb data
*
* Return:
*       IMP_SUCCESS   success
*
***********************************************************************/
IMP_U32 IMP_VFD_TK_YuvtRgb(IMAGE3_S *frame, IMP_U8 *rgb)//no more than two
{
	IMP_U8 *y_buffer, *u_buffer, *v_buffer;
	IMP_U8 u, v;
	IMP_U8 r, g, b;
	IMP_S32 i, j;
	IMP_S32 rgb_ptr, y_ptr;
	IMP_S32 width, height;
	IMP_S32 uv_site, j_2;

	width = frame->s32W;
	height = frame->s32H;

	y_buffer = frame->pu8D1;
	u_buffer = y_buffer + width * height;
	v_buffer = u_buffer + width * height / 4;

	rgb_ptr = 0;
	y_ptr = 0;

	/* compute rgb values using formula */
	for (j = 0; j < height; j++)
	{
		j_2 = j / 2;
		for (i = 0; i < width; i++)
		{
			uv_site = (width / 2)* j_2 + i / 2;

			u = u_buffer[uv_site] - 128;
			v = v_buffer[uv_site] - 128;

			/* shifting instead of +-x/ */
			r = y_buffer[y_ptr] + v + ((v * 103) >> 8);
			g = y_buffer[y_ptr] - ((u * 88) >> 8) - ((v * 183) >> 8);
			b = y_buffer[y_ptr] + u + ((u * 198) >> 8);

			r = MAX_2(0, MIN_2(255, r));
			g = MAX_2(0, MIN_2(255, g));
			b = MAX_2(0, MIN_2(255, b));

			rgb[rgb_ptr] = r;
			rgb[rgb_ptr + 1] = g;
			rgb[rgb_ptr + 2] = b;

			y_ptr++;
			rgb_ptr += 3;
		}
	}

	return IMP_SUCCESS;

}

/***********************************************************************
* Function:      IMP_VFD_TK_overlap
* Description: 	 calculate overlap of two rectangles
*
* Input:
*	   rect_1		rectangle 1
*	   rect_2		rectangle 2
*
* Output:
*		IMP_S32   	overlap*100/( area of bigger rectangle)
*
* Return:
*       IMP_S32   	overlap*100 /( area of bigger rectangle)
*
***********************************************************************/
IMP_S32 IMP_VFD_TK_overlap(IMP_RECT_S stRect1, IMP_RECT_S stRect2)
{
	IMP_S16 overr, overc;
	IMP_S16 r1, r2;

	r1 = MAX_2(stRect1.s16Y2 - stRect1.s16Y1, stRect1.s16X2 - stRect1.s16X1);
	r2 = MAX_2(stRect2.s16Y2 - stRect2.s16Y1, stRect2.s16X2 - stRect2.s16X1);
	if (r1 <= 0 || r2 <= 0)
	{
		return 0;
	}

	overr = MAX_2(0, MIN_2(stRect1.s16X2, stRect2.s16X2) - MAX_2(stRect1.s16X1, stRect2.s16X1));
	overc = MAX_2(0, MIN_2(stRect1.s16Y2, stRect2.s16Y2) - MAX_2(stRect1.s16Y1, stRect2.s16Y1));

	return MIN_2(((overr*overc) << 7) / (MIN_2(r1, r2)*MIN_2(r1, r2)), 128);
}

/*******************************************************************************************
* Function:      IMP_VFD_TK_cmp
* Description: 	 compare the new detections with tracking targets
*
* Input:
*		*pstTKPara	 tracking parameters
*
* Output:
*		*pstTKPara  	updated tracking width and height or new tracking target
*						comfirm to show a target
*
* Return:
*       IMP_SUCCESS     success
*
*	Others:
*	  Mainly Use:
*	  	s32VecDtc		detect vector,[0] is the number of detection
*	   	u8TkNum			the number of tracking targets
*		u8Confd			the confidence of each targets
*	    u8Confds		the setting confidence for tracking confirmation
*	    u8VecRoiIndx	the vector obtains roi index after comparation
*
********************************************************************************************/

IMP_U32 IMP_VFD_TK_cmp(IMP_VFD_TK_PARA_S *pstTKPara)
{
	IMP_S32 i, j;
	IMP_S32 overlap, last_olp;
	IMP_S32 flag;
	IMP_S32 new_ob_num;
	IMP_S32 new_x, new_y, new_y2, new_x2;
	IMP_S32 count;
	IMP_RECT_S rect_1;


	pstTKPara->u8VecRoiIndx[0] = 0;
	pstTKPara->u8VecRoiIndx[1] = 0;
	pstTKPara->u8VecRoiIndx[2] = 0;
	pstTKPara->u8VecRoiIndx[3] = 0;

	new_ob_num = pstTKPara->s32VecDtc[0];
	count = 4;

	/*	tracking targets compare to detections */


	for (i = 0; i < new_ob_num; i++)
	{
		flag = 0;
		for (j = 0; j < pstTKPara->u8TkNum; j++)
		{
			rect_1.s16X1 = pstTKPara->s32VecDtc[i * 4 + 1];
			rect_1.s16Y1 = pstTKPara->s32VecDtc[i * 4 + 2];
			rect_1.s16X2 = pstTKPara->s32VecDtc[i * 4 + 3];
			rect_1.s16Y2 = pstTKPara->s32VecDtc[i * 4 + 4];

			overlap = IMP_VFD_TK_overlap(rect_1, pstTKPara->stVecTarget[j].stROI);

			if (overlap > 26)
			{
				flag = 1;
			}
		}
		/*
		if there is no target overlapping with new detection
		means that is a new target, keep it in RoiVec
		*/
		if (flag == 0)
		{
			pstTKPara->u8VecRoiIndx[1] = 1;		/*new target flag*/
			pstTKPara->u8VecRoiIndx[3] += 1;		/*how many new targets*/
			pstTKPara->u8VecRoiIndx[count++] = i;/*which is the new target in the detections*/
		}

	}

	/*detections compare to the targets*/

	for (j = 0; j < pstTKPara->u8TkNum; j++)
	{
		last_olp = 26; flag = 0;
		for (i = 0; i < new_ob_num; i++)
		{
			rect_1.s16X1 = pstTKPara->s32VecDtc[i * 4 + 1];
			rect_1.s16Y1 = pstTKPara->s32VecDtc[i * 4 + 2];
			rect_1.s16X2 = pstTKPara->s32VecDtc[i * 4 + 3];
			rect_1.s16Y2 = pstTKPara->s32VecDtc[i * 4 + 4];

			overlap = IMP_VFD_TK_overlap(rect_1, pstTKPara->stVecTarget[j].stROI);

			if (overlap > last_olp)
			{
				last_olp = overlap;
				new_x2 = pstTKPara->s32VecDtc[i * 4 + 3];
				new_y2 = pstTKPara->s32VecDtc[i * 4 + 4];
				new_x = pstTKPara->s32VecDtc[i * 4 + 1];
				new_y = pstTKPara->s32VecDtc[i * 4 + 2];
				flag = 1;
			}
		}
		/*
		if this target is overlapping with any detections
		means it tracks well, increse its credit
		and increse the confidence
		if it arrives at the confidence level we set,
		then it can be confirmed to be a real target
		*/
		if (flag == 1)
		{
			pstTKPara->stVecTarget[j].s32Cred += 1;
			if (pstTKPara->stVecTarget[j].s32Cred >= 20)
			{
				pstTKPara->stVecTarget[j].s32Cred = 20;
			}
			pstTKPara->stVecTarget[j].u8Confd++;
			if (pstTKPara->stVecTarget[j].u8Confd >= pstTKPara->u8Confds)
				pstTKPara->stVecTarget[j].u8Showflg = 1;

			/*   update tracking target with detection( width and height) */

			if (pstTKPara->stVecTarget[j].u8Crsflg == 0)
			{
				pstTKPara->stVecTarget[j].u8Dtcflg = 1;
				pstTKPara->stVecTarget[j].stROI.s16X1 = new_x;
				pstTKPara->stVecTarget[j].stROI.s16Y1 = new_y;
				pstTKPara->stVecTarget[j].stROI.s16X2 = new_x2;
				pstTKPara->stVecTarget[j].stROI.s16Y2 = new_y2;

				pstTKPara->u8VecRoiIndx[0] = 1;
				pstTKPara->u8VecRoiIndx[2] += 1;
				pstTKPara->u8VecRoiIndx[count++] = j;
			}
		}
		/*
		if this target is not overlapping with any detections
		means it may be lost or disappare, decrease its credit
		*/
		else
			pstTKPara->stVecTarget[j].s32Cred -= pstTKPara->u8Sense;
	}
	return IMP_SUCCESS;
}

/***********************************************************************
* Function:   IMP_VFD_TK_init_tar
* Description: 	 initialize a new target
*
* Input:
*		*pstTKPara	 tracking parameters
*
* Output:
*		*pstTKPara	 initial targets' width and height from detection
*					 initial showflg and confidence
*
* Return:
*       IMP_SUCCESS      success
*
***********************************************************************/

IMP_U32 IMP_VFD_TK_init_tar(IMP_VFD_TK_PARA_S *pstTKPara)
{
	IMP_U8 arr_index, detect_index;

	arr_index = pstTKPara->u8TkIndx;
	detect_index = pstTKPara->u8DtcIndx;

	pstTKPara->stVecTarget[arr_index].u8Showflg = 0;
	pstTKPara->stVecTarget[arr_index].u8Confd = 0;
	pstTKPara->stVecTarget[arr_index].u32ID = ++(pstTKPara->u32TkID);

	pstTKPara->stVecTarget[arr_index].stROI.s16X2 = pstTKPara->s32VecDtc[detect_index * 4 + 3];
	pstTKPara->stVecTarget[arr_index].stROI.s16Y2 = pstTKPara->s32VecDtc[detect_index * 4 + 4];
	pstTKPara->stVecTarget[arr_index].stROI.s16X1 = pstTKPara->s32VecDtc[detect_index * 4 + 1];
	pstTKPara->stVecTarget[arr_index].stROI.s16Y1 = pstTKPara->s32VecDtc[detect_index * 4 + 2];

	return IMP_SUCCESS;
}

/***********************************************************************
* Function:   	 IMP_VFD_TK_init_ms
* Description: 	 initialize the meanshift features of target
*
* Input:
*		*pstTKPara		tracking parameters
*
* Output:
*		*pstTKPara		 update targets' meanshift Weight
*						 and its rgb Histogram feature
*
* Return:
*       IMP_SUCCESS      success
* Others:
*	Mainly use:
*
*		pf32MsftWei		each target meanshift feature weight
*		pu8RgbBuf		rgb data
*		f32VecTkHist	each tracking histogram feature
***********************************************************************/

IMP_U32 IMP_VFD_TK_init_ms(IMP_VFD_TK_PARA_S *pstTKPara)
{
	IMP_S32 t_h, t_w, t_x, t_y;
	IMP_S32 i, j;
	IMP_S32 q_r, q_g, q_b, q_temp;
	IMP_FLOAT h;
	IMP_FLOAT dist;
	IMP_S32 width, height;
	IMP_U8  index;

	index = pstTKPara->u8TkIndx;

	width = pstTKPara->stWinSz.s16W;
	height = pstTKPara->stWinSz.s16H;

	t_h = pstTKPara->stVecTarget[index].stROI.s16Y2 - pstTKPara->stVecTarget[index].stROI.s16Y1;
	t_w = pstTKPara->stVecTarget[index].stROI.s16X2 - pstTKPara->stVecTarget[index].stROI.s16X1;
	t_x = pstTKPara->stVecTarget[index].stROI.s16X1;
	t_y = pstTKPara->stVecTarget[index].stROI.s16Y1;

	memset(pstTKPara->stVecTarget[index].pf32MsftWei, 0, sizeof(IMP_FLOAT)* width * height);
	memset(pstTKPara->stVecTarget[index].f32VecTkHist, 0, sizeof(IMP_FLOAT)*NUM_BINS);

	h = ((IMP_FLOAT)t_w / 2)* ((IMP_FLOAT)t_w / 2) + ((IMP_FLOAT)t_h / 2)*((IMP_FLOAT)t_h / 2);

	/* update target weight feature */
	for (i = 0; i < t_h; i++)
	{
		for (j = 0; j < t_w; j++)
		{
			dist = (i - (IMP_FLOAT)t_h / 2)*(i - (IMP_FLOAT)t_h / 2) +
				(j - (IMP_FLOAT)t_w / 2)*(j - (IMP_FLOAT)t_w / 2);

			pstTKPara->stVecTarget[index].pf32MsftWei[i * t_w + j] = (IMP_FLOAT)(1.0 - dist / h);
		}
	}


	/* update target histogram feature */
	for (i = t_y; i < t_y + t_h; i++)
	{
		for (j = t_x; j < t_x + t_w; j++)
		{
			q_r = ((pstTKPara->pu8RgbBuf[(i*width + j) * 3]) >> 5);
			q_g = ((pstTKPara->pu8RgbBuf[(i*width + j) * 3 + 1])>> 5);
			q_b = ((pstTKPara->pu8RgbBuf[(i*width + j) * 3 + 2]) >> 5 );
			q_temp = q_r * 64 + (q_g << 3) + q_b;

			pstTKPara->stVecTarget[index].f32VecTkHist[q_temp] = pstTKPara->stVecTarget[index].f32VecTkHist[q_temp]
				+ pstTKPara->stVecTarget[index].pf32MsftWei[(i - t_y) * t_w + (j - t_x)];
		}
	}
	return IMP_SUCCESS;
}

/***********************************************************************
* Function:   	 IMP_VFD_TK_add_tar
* Description: 	 add a new target
*
* Input:
*	    *pstTKPara		tracking parameters
*
*
* Output:
*		*pstTKPara		 initial width height credit for a new target
*						 also the meanshift features
*
* Return:
*       IMP_SUCCESS      success
*
***********************************************************************/

IMP_U32 IMP_VFD_TK_add_tar(IMP_VFD_TK_PARA_S *pstTKPara)
{
	pstTKPara->stVecTarget[pstTKPara->u8TkIndx].s32Cred = 0;
	pstTKPara->stVecTarget[pstTKPara->u8TkIndx].u8Showflg = 0;
	pstTKPara->stVecTarget[pstTKPara->u8TkIndx].u8Dtcflg = 1;

	IMP_VFD_TK_init_tar(pstTKPara);
	IMP_VFD_TK_init_ms(pstTKPara);

	return IMP_SUCCESS;
}

/***********************************************************************
* Function:   	 IMP_VFD_TK_find_indx
* Description: 	 use target its index find target[index]<-
*
* Input:
*	   TRACK_PRO      *tar			targets
*	   IMP_S32	 	  index    		the index of tracking target in detection array
*	   IMP_S32 		  max_num		max tracking number
*
* Output:
*		IMP_S32			target index
*
* Return:
*	    IMP_S32         target index
*       IMP_FALSE     	false
*
*	Others:
*		Used by inner functions
***********************************************************************/

IMP_S32 IMP_VFD_TK_find_indx(IMP_VFD_TK_TARGET_S *tar, IMP_S32 index, IMP_S32 max_num)
{
	IMP_S32 i;
	for (i = 0; i < max_num; i++)
		if (tar[i].u8Indx == index) return i;
	return IMP_FALSE;
}

/***********************************************************************
* Function:   	 IMP_VFD_TK_moveleft
* Description: 	 move forward the targets behind the delete one
*
* Input:
*	   TRACK_PRO      *tar			targets
*	   IMP_S32	 	  delet_indx    the index of delete target
*	   IMP_S32 		  final_indx	the final target index
*
* Output:
*		TRACK_PRO		tar			changed targets
*
* Return:
*	    IMP_SUCCESS      success
*
* Others:
*		Used by inner functions
***********************************************************************/
IMP_U32 IMP_VFD_TK_moveleft(IMP_VFD_TK_TARGET_S *tar, IMP_S32 delet_indx, IMP_S32 final_indx)
{
	IMP_S32 i;
	IMP_VFD_TK_TARGET_S t = tar[delet_indx];
	for (i = delet_indx; i < final_indx; i++)
	{
		tar[i] = tar[i + 1];
	}
	tar[final_indx] = t;
	return IMP_SUCCESS;
}

/***********************************************************************
* Function:   	 IMP_VFD_TK_delete_tar
* Description: 	 delete a target
*
* Input:
*	    *pstTKPara		tracking parameters
*		 delet_indx		the index of delete target
*
* Output:
*		*pstTKPara		move the delete target to the end,
*						and move forward others
*
* Return:
*	    IMP_SUCCESS      success
*
*
***********************************************************************/

IMP_U32 IMP_VFD_TK_delete_tar(IMP_VFD_TK_PARA_S *pstTKPara, IMP_S32 delet_indx)
{

	IMP_S32 delet_t, final_t, final_index;

	final_index = pstTKPara->stVecTarget[pstTKPara->u8TkNum].u8Indx;
	delet_t = IMP_VFD_TK_find_indx(pstTKPara->stVecTarget, delet_indx, MAXNDETECTIONS);
	final_t = IMP_VFD_TK_find_indx(pstTKPara->stVecTarget, final_index, MAXNDETECTIONS);

	if (final_t > delet_t)
		IMP_VFD_TK_moveleft(pstTKPara->stVecTarget, delet_t, final_t);

	return IMP_SUCCESS;
}
/***********************************************************************
* Function:   	 IMP_VFD_TK_meanishift
* Description: 	 meanshift method for tracking, move to the new position
*
* Input:
*	   *pstTKPara		tracking parameters
*
* Output:
*		*pstTKPara		update target to a new position
*
* Return:
*	    IMP_SUCCESS     success
*
***********************************************************************/
IMP_U32 IMP_VFD_TK_meanishift(IMP_VFD_TK_PARA_S *pstTKPara){

	IMP_S32 num, i, j;
	IMP_S32 t_w, t_h, t_x, t_y;
	IMP_FLOAT  x1, x2, y1, y2;
	IMP_FLOAT sum_w;
	IMP_S32 q_r, q_g, q_b;
	IMP_U8 index;

	num = 0; y1 = 2.0; y2 = 2.0;
	index = pstTKPara->u8TkIndx;
	t_h = pstTKPara->stVecTarget[index].stROI.s16Y2 - pstTKPara->stVecTarget[index].stROI.s16Y1;
	t_w = pstTKPara->stVecTarget[index].stROI.s16X2 - pstTKPara->stVecTarget[index].stROI.s16X1;

	/* meanshift iteration conditions */
	while ((y2*y2 + y1*y1 > DIST) && (num<pstTKPara->u16IteraNum))
	{

		num++;
		t_x = pstTKPara->stVecTarget[index].stROI.s16X1;
		t_y = pstTKPara->stVecTarget[index].stROI.s16Y1;

		/* boundary protection */
		if (t_x <= 0 || t_x + t_w >= pstTKPara->stWinSz.s16W)
		{
			t_x = 0; t_y = 0;
		}
		if (t_y <= 0 || t_y + t_h >= pstTKPara->stWinSz.s16H)
		{
			t_y = 0; t_x = 0;
		}

		memset(pstTKPara->pu16RgbTmp, 0, sizeof(IMP_U16)*t_w*t_h);
		memset(pstTKPara->f32VecHistTmp, 0, sizeof(IMP_FLOAT)*NUM_BINS);
		memset(pstTKPara->f32VecWeiTmp, 0, sizeof(IMP_FLOAT)*NUM_BINS);

		/* use each target weight feature */
		for (i = t_y; i < t_h + t_y; i++)
		{
			for (j = t_x; j < t_w + t_x; j++)
			{
				q_r = (pstTKPara->pu8RgbBuf[(i* (pstTKPara->stWinSz.s16W) + j) * 3]) >> 5;
				q_g = (pstTKPara->pu8RgbBuf[(i* (pstTKPara->stWinSz.s16W) + j) * 3 + 1]) >>5;
				q_b = (pstTKPara->pu8RgbBuf[(i* (pstTKPara->stWinSz.s16W) + j) * 3 + 2]) >>5;
				pstTKPara->pu16RgbTmp[(i - t_y) *t_w + j - t_x] = q_r * 64 + (q_g <<3) + q_b;

				pstTKPara->f32VecHistTmp[pstTKPara->pu16RgbTmp[(i - t_y) *t_w + j - t_x]] =
					pstTKPara->f32VecHistTmp[pstTKPara->pu16RgbTmp[(i - t_y) *t_w + j - t_x]] +
					pstTKPara->stVecTarget[index].pf32MsftWei[(i - t_y) * t_w + j - t_x];
			}
		}

		/* calculate center of "gravity" */
		for (i = 0; i < NUM_BINS; i++)
		{
			if (pstTKPara->f32VecHistTmp[i] != 0)
				pstTKPara->f32VecWeiTmp[i] = (IMP_FLOAT)sqrt(pstTKPara->stVecTarget[index].f32VecTkHist[i] / pstTKPara->f32VecHistTmp[i]);
			else
				pstTKPara->f32VecWeiTmp[i] = 0;
		}

		sum_w = 0.0;
		x1 = 0.0;
		x2 = 0.0;

		/* get shift of target move from formula */
		for (i = 0; i < t_h; i++)
		{
			for (j = 0; j < t_w; j++)
			{
				sum_w = sum_w + pstTKPara->f32VecWeiTmp[pstTKPara->pu16RgbTmp[i * t_w + j]];/* m00 */
				x1 = x1 + pstTKPara->f32VecWeiTmp[pstTKPara->pu16RgbTmp[i * t_w + j]] * (i - t_h / 2);/* m10 */
				x2 = x2 + pstTKPara->f32VecWeiTmp[pstTKPara->pu16RgbTmp[i * t_w + j]] * (j - t_w / 2);/* m01 */
			}
		}

		y1 = x1 / sum_w;
		y2 = x2 / sum_w;
		/* boundry protection */
		if (pstTKPara->stVecTarget[index].stROI.s16X1 + y2 < pstTKPara->stWinSz.s16W
			&& pstTKPara->stVecTarget[index].stROI.s16Y1 + y1 < pstTKPara->stWinSz.s16H
			&&pstTKPara->stVecTarget[index].stROI.s16X1 + y2 >0
			&& pstTKPara->stVecTarget[index].stROI.s16Y1 + y1>0 && t_x>0 && t_x>0)

			/* update position */
		{
			pstTKPara->stVecTarget[index].stROI.s16X1 += (IMP_S16)(y2);
			pstTKPara->stVecTarget[index].stROI.s16Y1 += (IMP_S16)(y1);
			pstTKPara->stVecTarget[index].stROI.s16X2 = pstTKPara->stVecTarget[index].stROI.s16X1 + t_w;
			pstTKPara->stVecTarget[index].stROI.s16Y2 = pstTKPara->stVecTarget[index].stROI.s16Y1 + t_h;

		}
		else
		{
			pstTKPara->stVecTarget[index].stROI.s16X1 = 0;
			pstTKPara->stVecTarget[index].stROI.s16Y1 = 0;
			break;
		}
	}


	return IMP_SUCCESS;
}
/***********************************************************************
* Function:   	 IMP_VFD_TK_cross_tar
*
* Description: 	 if the targets cross to each other, make it disappear
*
* Input:
*		*tar				targets
*		 tracking_num     	now the number of tracking
*
* Output:
*		 u8Crsflg			cross flag setting
*		 s32Cred			setting cross target credit as -1000
*							when detect, it will be deleted
*
* Return:
*	     IMP_SUCCESS		success
*
***********************************************************************/
IMP_S32 IMP_VFD_TK_cross_tar(IMP_S32 tracking_num, IMP_VFD_TK_TARGET_S* tar)
{
	IMP_S32 i, j;
	IMP_S32 flag = 0;
	IMP_S32 overlap;

	for (i = 0; i < tracking_num; i++)
		for (j = i + 1; j < tracking_num; j++)
		{
			overlap = IMP_VFD_TK_overlap(tar[i].stROI, tar[j].stROI);

			if (overlap >= 57){
				tar[i].s32Cred -= 1000;
				tar[j].s32Cred -= 1000;
			}

			if (overlap < 26)
			{
				tar[j].u8Crsflg = 0; tar[i].u8Crsflg = 0;
			}
			else
			{
				tar[j].u8Crsflg = 1; tar[i].u8Crsflg = 1;
			}
		}
	return IMP_SUCCESS;

}


/***********************************************************************
* Function:    IMP_VFD_TK_release
* Description: tracking parameters memory free
* Input:
*		*pstTKPara	 tracking parameters
*
* Output:
*		*pstTKPara	 free malloc tracking parameters
* Return:
*		 free status
***********************************************************************/
IMP_S32 IMP_VFD_TK_release(IMP_VFD_TK_PARA_S *pstTKPara)
{
	IMP_U8 i;
	for (i = 0; i < MAXNDETECTIONS; i++)
	{
		free(pstTKPara->stVecTarget[i].pf32MsftWei);
	}
	free(pstTKPara->pu16RgbTmp);
	free(pstTKPara->pu8RgbBuf);
	return IMP_SUCCESS;
}





IMP_U32 IMP_VFD_TK_color(IMP_HANDLE hModule, IMAGE3_S *pstImage)
{
	static int alreadytrack = 0;
	static int flag = 0;
	int k, i, j, t;
	IMP_U8 stVecDelete[MAXNDETECTIONS];
	IMP_U8 stVecDltIndx[MAXNDETECTIONS];

	IMP_HANDLE *m_ptr = (IMP_HANDLE *)hModule;
	VFD_MODULE_S *m_hPara = (VFD_MODULE_S*)m_ptr[0];
	IMP_VFD_PARA_S *m_Para = (IMP_VFD_PARA_S *)m_ptr[1];

	if (!alreadytrack)
	{
		for (i = 0; i < m_hPara->trackpara.s32VecDtc[0]; i++)
		{
			IMP_VFD_TK_YuvtRgb(pstImage, m_hPara->trackpara.pu8RgbBuf);
			m_hPara->trackpara.u8DtcIndx = i;
			m_hPara->trackpara.u8TkIndx = m_hPara->trackpara.u8TkNum;
			IMP_VFD_TK_init_tar(&m_hPara->trackpara);
			IMP_VFD_TK_init_ms(&m_hPara->trackpara);

			m_hPara->trackpara.u8TkNum++;
			flag = 1;
		}
	}

	if (flag == 1)
	{
		alreadytrack = 1;
		IMP_VFD_TK_YuvtRgb(pstImage, m_hPara->trackpara.pu8RgbBuf);
		/*track*/

		k = 0;
		for (i = 0; i <m_hPara->trackpara.u8TkNum; i++)
		{
			if (m_hPara->trackpara.stVecTarget[i].stROI.s16X1 <= 0
				|| m_hPara->trackpara.stVecTarget[i].stROI.s16Y1 <= 0
				|| m_hPara->trackpara.stVecTarget[i].stROI.s16X2>pstImage->s32W
				|| m_hPara->trackpara.stVecTarget[i].stROI.s16Y2>pstImage->s32H)

			{
				stVecDelete[k++] = i;
			}
		}
		for (i = 0; i < k; i++)
		{
			stVecDltIndx[i] = m_hPara->trackpara.stVecTarget[stVecDelete[i]].u8Indx;
		}
		for (j = 0; j < k; j++)
			for (i = 0; i < k - 1 - j; i++)
			{
				if (stVecDltIndx[i] > stVecDltIndx[i + 1])
				{
					t = stVecDltIndx[i];
					stVecDltIndx[i] = stVecDltIndx[i + 1];
					stVecDltIndx[i + 1] = t;
				}
			}
		for (i = 0; i < k; i++)
		{
			IMP_VFD_TK_delete_tar(&m_hPara->trackpara, stVecDltIndx[i]);
			m_hPara->trackpara.u8TkNum--;
		}

		for (i = 0; i < m_hPara->trackpara.u8TkNum; i++)
		{
			if (m_hPara->trackpara.u8Confds == 0)
			{
				m_hPara->trackpara.stVecTarget[i].u8Showflg = 1;
			}

			m_hPara->trackpara.u8TkIndx = i;
			IMP_VFD_TK_meanishift(&m_hPara->trackpara);
		}

		k = 0;
		for (i = 0; i <m_hPara->trackpara.u8TkNum; i++)
		{
			if (m_hPara->trackpara.stVecTarget[i].stROI.s16X1 <= 0
				|| m_hPara->trackpara.stVecTarget[i].stROI.s16Y1 <= 0
				|| m_hPara->trackpara.stVecTarget[i].stROI.s16X2>pstImage->s32W
				|| m_hPara->trackpara.stVecTarget[i].stROI.s16Y2>pstImage->s32H)

			{
				stVecDelete[k++] = i;
			}
		}
		for (i = 0; i < k; i++)
		{
			stVecDltIndx[i] = m_hPara->trackpara.stVecTarget[stVecDelete[i]].u8Indx;
		}
		for (j = 0; j < k; j++)
			for (i = 0; i < k - 1 - j; i++)
			{
				if (stVecDltIndx[i] < stVecDltIndx[i + 1])
				{
					t = stVecDltIndx[i];
					stVecDltIndx[i] = stVecDltIndx[i + 1];
					stVecDltIndx[i + 1] = t;
				}
			}
		for (i = 0; i < k; i++)
		{
			IMP_VFD_TK_delete_tar(&m_hPara->trackpara, stVecDltIndx[i]);
			m_hPara->trackpara.u8TkNum--;
		}

		IMP_VFD_TK_cross_tar(m_hPara->trackpara.u8TkNum, m_hPara->trackpara.stVecTarget);

		k = 0;
		for (i = 0; i <m_hPara->trackpara.u8TkNum; i++)
		{
			if (m_hPara->trackpara.stVecTarget[i].s32Cred < -20
				|| (m_hPara->trackpara.stVecTarget[i].stROI.s16X2)>pstImage->s32W
				|| (m_hPara->trackpara.stVecTarget[i].stROI.s16Y2)>pstImage->s32H
				|| m_hPara->trackpara.stVecTarget[i].stROI.s16X1 <= 0
				|| m_hPara->trackpara.stVecTarget[i].stROI.s16Y1 <= 0)

			{
				stVecDelete[k++] = i;
			}
		}
		for (i = 0; i < k; i++)
		{
			stVecDltIndx[i] = m_hPara->trackpara.stVecTarget[stVecDelete[i]].u8Indx;
		}
		for (j = 0; j < k; j++)
			for (i = 0; i < k - 1 - j; i++)
			{
				if (stVecDltIndx[i] < stVecDltIndx[i + 1])
				{
					t = stVecDltIndx[i];
					stVecDltIndx[i] = stVecDltIndx[i + 1];
					stVecDltIndx[i + 1] = t;
				}
			}
		for (i = 0; i < k; i++)
		{
			IMP_VFD_TK_delete_tar(&m_hPara->trackpara, stVecDltIndx[i]);
			m_hPara->trackpara.u8TkNum--;
		}

		if (m_hPara->fr % m_Para->u32DetectFQ == 0)
		{
			for (i = 0; i < m_hPara->trackpara.u8TkNum; i++)
			{
				m_hPara->trackpara.stVecTarget[i].u8Dtcflg = 0;
			}

			IMP_VFD_TK_cmp(&m_hPara->trackpara);


			/**/
			if (m_hPara->trackpara.u8VecRoiIndx[1] = 1)
			{
				for (i = 0; i < m_hPara->trackpara.u8VecRoiIndx[3]; i++)
				{
					if (m_hPara->trackpara.u8TkNum < MAXNDETECTIONS)
						if (m_hPara->trackpara.u8TkNum < m_Para->u32DetectNumax)
					{
						m_hPara->trackpara.u8TkIndx = m_hPara->trackpara.u8TkNum;
						m_hPara->trackpara.u8DtcIndx = m_hPara->trackpara.u8VecRoiIndx[i + 4];
						IMP_VFD_TK_add_tar(&m_hPara->trackpara);
						m_hPara->trackpara.u8TkNum++;
					}
				}
			}

			/**/
			if (m_hPara->trackpara.u8VecRoiIndx[0] = 1)
			{

				for (i = 0; i < m_hPara->trackpara.u8VecRoiIndx[2]; i++)
				{
					if (m_hPara->trackpara.u8TkNum <= MAXNDETECTIONS)
					{
						m_hPara->trackpara.u8TkIndx = i;
						IMP_VFD_TK_init_ms(&m_hPara->trackpara);
					}
				}
			}


		}
		k = 0;
		j = 0;
		for (i = 0; i < m_hPara->trackpara.u8TkNum; i++)
		{
			if (m_hPara->trackpara.stVecTarget[i].s32Cred < -15
				|| (m_hPara->trackpara.stVecTarget[i].stROI.s16X2)>pstImage->s32W
				|| (m_hPara->trackpara.stVecTarget[i].stROI.s16Y2)>pstImage->s32H
				|| m_hPara->trackpara.stVecTarget[i].stROI.s16X1 <= 0 || m_hPara->trackpara.stVecTarget[i].stROI.s16Y1 <= 0)
			{
				stVecDelete[k++] = i;
			}
		}
		for (i = 0; i < k; i++)
		{
			stVecDltIndx[i] = m_hPara->trackpara.stVecTarget[stVecDelete[i]].u8Indx;
		}
		for (j = 0; j < k; j++)
			for (i = 0; i < k - 1 - j; i++)
			{
				if (stVecDltIndx[i] < stVecDltIndx[i + 1])
				{
					t = stVecDltIndx[i];
					stVecDltIndx[i] = stVecDltIndx[i + 1];
					stVecDltIndx[i + 1] = t;
				}
			}
		for (i = 0; i < k; i++)
		{
			IMP_VFD_TK_delete_tar(&m_hPara->trackpara, stVecDltIndx[i]);
			m_hPara->trackpara.u8TkNum--;
		}


		m_hPara->m_fiOut.s32Facenumber = m_hPara->trackpara.u8TkNum;

		for (i = 0; i < m_hPara->trackpara.u8TkNum; i++)
		{
			m_hPara->m_fiOut.stFace[i].u32FaceID = m_hPara->trackpara.stVecTarget[i].u32ID;
			m_hPara->m_fiOut.stFace[i].stPosition.s16X1 = m_hPara->trackpara.stVecTarget[i].stROI.s16X1;
			m_hPara->m_fiOut.stFace[i].stPosition.s16X2 = m_hPara->trackpara.stVecTarget[i].stROI.s16X2;
			m_hPara->m_fiOut.stFace[i].stPosition.s16Y1 = m_hPara->trackpara.stVecTarget[i].stROI.s16Y1;
			m_hPara->m_fiOut.stFace[i].stPosition.s16Y2 = m_hPara->trackpara.stVecTarget[i].stROI.s16Y2;
			m_hPara->m_fiOut.stFace[i].flagShow = m_hPara->trackpara.stVecTarget[i].u8Showflg;

		}

	}

	return IMP_SUCCESS;
}


/*gray*/

IMP_U32 IMP_VFD_TK_init_Gray_ms(IMP_VFD_TK_PARA_S *pstTKPara)
{
	IMP_S32 t_h, t_w, t_x, t_y;
	IMP_S32 i, j;
	IMP_S32  q_temp;
	IMP_FLOAT h;
	IMP_FLOAT dist;
	IMP_S32 width, height;
	IMP_U8  index;

	index = pstTKPara->u8TkIndx;

	width = pstTKPara->stWinSz.s16W;
	height = pstTKPara->stWinSz.s16H;

	t_h = pstTKPara->stVecTarget[index].stROI.s16Y2 - pstTKPara->stVecTarget[index].stROI.s16Y1;
	t_w = pstTKPara->stVecTarget[index].stROI.s16X2 - pstTKPara->stVecTarget[index].stROI.s16X1;
	t_x = pstTKPara->stVecTarget[index].stROI.s16X1;
	t_y = pstTKPara->stVecTarget[index].stROI.s16Y1;

	memset(pstTKPara->stVecTarget[index].pf32MsftWei, 0, sizeof(IMP_FLOAT)* width * height);
	memset(pstTKPara->stVecTarget[index].f32VecTkHist, 0, sizeof(IMP_FLOAT) * 256);

	h = ((IMP_FLOAT)t_w / 2)* ((IMP_FLOAT)t_w / 2) + ((IMP_FLOAT)t_h / 2)*((IMP_FLOAT)t_h / 2);

	/* update target weight feature */
	for (i = 0; i < t_h; i++)
	{
		for (j = 0; j < t_w; j++)
		{
			dist = (i - (IMP_FLOAT)t_h / 2)*(i - (IMP_FLOAT)t_h / 2) +
				(j - (IMP_FLOAT)t_w / 2)*(j - (IMP_FLOAT)t_w / 2);

			pstTKPara->stVecTarget[index].pf32MsftWei[i * t_w + j] = (IMP_FLOAT)(1.0 - dist / h);
#if 0
			if (pstTKPara->stVecTarget[index].pf32MsftWei[i * t_w + j]>0)
				printf("%f", pstTKPara->stVecTarget[index].pf32MsftWei[i * t_w + j]);
#endif
		}
	}


	/* update target histogram feature */
	for (i = t_y; i < t_y + t_h; i++)
	{
		for (j = t_x; j < t_x + t_w; j++)
		{
			
			q_temp = pstTKPara->pu8RgbBuf[(i*width + j)];
			pstTKPara->stVecTarget[index].f32VecTkHist[q_temp] = pstTKPara->stVecTarget[index].f32VecTkHist[q_temp]
				+ pstTKPara->stVecTarget[index].pf32MsftWei[(i - t_y) * t_w + (j - t_x)];
#if 0
			if (pstTKPara->stVecTarget[index].f32VecTkHist[q_r]>0)
				printf("%f", pstTKPara->stVecTarget[index].f32VecTkHist[q_r]);
#endif
		}
	}
	return IMP_SUCCESS;
}


IMP_U32 IMP_VFD_TK_Gray_meanishift(IMP_VFD_TK_PARA_S *pstTKPara){

	IMP_S32 num, i, j;
	IMP_S32 t_w, t_h, t_x, t_y;
	IMP_FLOAT  x1, x2, y1, y2;
	IMP_FLOAT sum_w;
	IMP_U8 index;

	num = 0; y1 = 1.0; y2 = 1.0;
	index = pstTKPara->u8TkIndx;
	t_h = pstTKPara->stVecTarget[index].stROI.s16Y2 - pstTKPara->stVecTarget[index].stROI.s16Y1;
	t_w = pstTKPara->stVecTarget[index].stROI.s16X2 - pstTKPara->stVecTarget[index].stROI.s16X1;

	/* meanshift iteration conditions */
	while ((y2*y2 + y1*y1 > DIST) && (num<pstTKPara->u16IteraNum))
	{

		num++;
		t_x = pstTKPara->stVecTarget[index].stROI.s16X1;
		t_y = pstTKPara->stVecTarget[index].stROI.s16Y1;

		/* boundary protection */
		if (t_x <= 0 || t_x + t_w >= pstTKPara->stWinSz.s16W)
		{
			t_x = 0; t_y = 0;
		}
		if (t_y <= 0 || t_y + t_h >= pstTKPara->stWinSz.s16H)
		{
			t_y = 0; t_x = 0;
		}

		memset(pstTKPara->pu16RgbTmp, 0, sizeof(IMP_U16)*t_w*t_h);
		memset(pstTKPara->f32VecHistTmp, 0, sizeof(IMP_FLOAT)*256);
		memset(pstTKPara->f32VecWeiTmp, 0, sizeof(IMP_FLOAT)*256);

		/* use each target weight feature */
		for (i = t_y; i < t_h + t_y; i++)
		{
			for (j = t_x; j < t_w + t_x; j++)
			{
				pstTKPara->pu16RgbTmp[(i - t_y) *t_w + j - t_x] = pstTKPara->pu8RgbBuf[(i * (pstTKPara->stWinSz.s16W) + j)];
				pstTKPara->f32VecHistTmp[pstTKPara->pu16RgbTmp[(i - t_y) *t_w + j - t_x]] =
					pstTKPara->f32VecHistTmp[pstTKPara->pu16RgbTmp[(i - t_y) *t_w + j - t_x]] +
					pstTKPara->stVecTarget[index].pf32MsftWei[(i - t_y) * t_w + j - t_x];

			}
		}

		/* calculate center of "gravity" */
		for (i = 0; i < 256; i++)
		{
			if (pstTKPara->f32VecHistTmp[i] != 0)
				pstTKPara->f32VecWeiTmp[i] = (IMP_FLOAT)sqrt(pstTKPara->stVecTarget[index].f32VecTkHist[i] / pstTKPara->f32VecHistTmp[i]);
			else
				pstTKPara->f32VecWeiTmp[i] = 0;
		}

		sum_w = 0.0;
		x1 = 0.0;
		x2 = 0.0;

		/* get shift of target move from formula */
		for (i = 0; i < t_h; i++)
		{
			for (j = 0; j < t_w; j++)
			{
				sum_w = sum_w + pstTKPara->f32VecWeiTmp[pstTKPara->pu16RgbTmp[i * t_w + j]];/* m00 */
				x1 = x1 + pstTKPara->f32VecWeiTmp[pstTKPara->pu16RgbTmp[i * t_w + j]] * (i - t_h / 2);/* m10 */
				x2 = x2 + pstTKPara->f32VecWeiTmp[pstTKPara->pu16RgbTmp[i * t_w + j]] * (j - t_w / 2);/* m01 */
			}
		}

		y1 = x1 / sum_w;
		y2 = x2 / sum_w;

		/* boundry protection */
		if (pstTKPara->stVecTarget[index].stROI.s16X1 + y2 < pstTKPara->stWinSz.s16W
			&& pstTKPara->stVecTarget[index].stROI.s16Y1 + y1 < pstTKPara->stWinSz.s16H
			&&pstTKPara->stVecTarget[index].stROI.s16X1 + y2 >0
			&& pstTKPara->stVecTarget[index].stROI.s16Y1 + y1>0 && t_x>0 && t_x>0)

			/* update position */
		{
			pstTKPara->stVecTarget[index].stROI.s16X1 += (IMP_S16)(y2);
			pstTKPara->stVecTarget[index].stROI.s16Y1 += (IMP_S16)(y1);
			pstTKPara->stVecTarget[index].stROI.s16X2 = pstTKPara->stVecTarget[index].stROI.s16X1 + t_w;
			pstTKPara->stVecTarget[index].stROI.s16Y2 = pstTKPara->stVecTarget[index].stROI.s16Y1 + t_h;

		}
		else
		{
			pstTKPara->stVecTarget[index].stROI.s16X1 = 0;
			pstTKPara->stVecTarget[index].stROI.s16Y1 = 0;
			break;
		}
	}


	return IMP_SUCCESS;
}
IMP_U32 IMP_VFD_TK_Gray_add_tar(IMP_VFD_TK_PARA_S *pstTKPara)
{
	pstTKPara->stVecTarget[pstTKPara->u8TkIndx].s32Cred = 0;
	pstTKPara->stVecTarget[pstTKPara->u8TkIndx].u8Showflg = 0;
	pstTKPara->stVecTarget[pstTKPara->u8TkIndx].u8Dtcflg = 1;

	IMP_VFD_TK_init_tar(pstTKPara);
	IMP_VFD_TK_init_Gray_ms(pstTKPara);

	return IMP_SUCCESS;
}
IMP_U32 IMP_VFD_TK_gray(IMP_HANDLE hModule, IMAGE3_S *pstImage)
{
	static int alreadytrack = 0;
	static int flag = 0;
	int k, i, j, t;
	IMP_U8 stVecDelete[MAXNDETECTIONS];
	IMP_U8 stVecDltIndx[MAXNDETECTIONS];

	IMP_HANDLE *m_ptr = (IMP_HANDLE *)hModule;
	VFD_MODULE_S *m_hPara = (VFD_MODULE_S*)m_ptr[0];
	IMP_VFD_PARA_S *m_Para = (IMP_VFD_PARA_S *)m_ptr[1];

	if (!alreadytrack)
	{
		for (i = 0; i < m_hPara->trackpara.s32VecDtc[0]; i++)
		{
			memcpy(m_hPara->trackpara.pu8RgbBuf, pstImage->pu8D1, pstImage->s32W*pstImage->s32H);
			m_hPara->trackpara.u8DtcIndx = i;
			m_hPara->trackpara.u8TkIndx = m_hPara->trackpara.u8TkNum;
			IMP_VFD_TK_init_tar(&m_hPara->trackpara);
			IMP_VFD_TK_init_Gray_ms(&m_hPara->trackpara);

			m_hPara->trackpara.u8TkNum++;
			flag = 1;
		}
	}

	if (flag == 1)
	{
		alreadytrack = 1;
		memcpy(m_hPara->trackpara.pu8RgbBuf, pstImage->pu8D1, pstImage->s32W*pstImage->s32H);
		/*track*/

		k = 0;
		for (i = 0; i <m_hPara->trackpara.u8TkNum; i++)
		{
			if (m_hPara->trackpara.stVecTarget[i].stROI.s16X1 <= 0
				|| m_hPara->trackpara.stVecTarget[i].stROI.s16Y1 <= 0
				|| m_hPara->trackpara.stVecTarget[i].stROI.s16X2>pstImage->s32W
				|| m_hPara->trackpara.stVecTarget[i].stROI.s16Y2>pstImage->s32H)

			{
				stVecDelete[k++] = i;
			}
		}
		for (i = 0; i < k; i++)
		{
			stVecDltIndx[i] = m_hPara->trackpara.stVecTarget[stVecDelete[i]].u8Indx;
		}
		for (j = 0; j < k; j++)
			for (i = 0; i < k - 1 - j; i++)
			{
				if (stVecDltIndx[i] > stVecDltIndx[i + 1])
				{
					t = stVecDltIndx[i];
					stVecDltIndx[i] = stVecDltIndx[i + 1];
					stVecDltIndx[i + 1] = t;
				}
			}
		for (i = 0; i < k; i++)
		{
			IMP_VFD_TK_delete_tar(&m_hPara->trackpara, stVecDltIndx[i]);
			m_hPara->trackpara.u8TkNum--;
		}

		for (i = 0; i < m_hPara->trackpara.u8TkNum; i++)
		{
			if (m_hPara->trackpara.u8Confds == 0)
			{
				m_hPara->trackpara.stVecTarget[i].u8Showflg = 1;
			}

			m_hPara->trackpara.u8TkIndx = i;
			IMP_VFD_TK_Gray_meanishift(&m_hPara->trackpara);
		}

		k = 0;
		for (i = 0; i <m_hPara->trackpara.u8TkNum; i++)
		{
			if (m_hPara->trackpara.stVecTarget[i].stROI.s16X1 <= 0
				|| m_hPara->trackpara.stVecTarget[i].stROI.s16Y1 <= 0
				|| m_hPara->trackpara.stVecTarget[i].stROI.s16X2>pstImage->s32W
				|| m_hPara->trackpara.stVecTarget[i].stROI.s16Y2>pstImage->s32H)

			{
				stVecDelete[k++] = i;
			}
		}
		for (i = 0; i < k; i++)
		{
			stVecDltIndx[i] = m_hPara->trackpara.stVecTarget[stVecDelete[i]].u8Indx;
		}
		for (j = 0; j < k; j++)
			for (i = 0; i < k - 1 - j; i++)
			{
				if (stVecDltIndx[i] < stVecDltIndx[i + 1])
				{
					t = stVecDltIndx[i];
					stVecDltIndx[i] = stVecDltIndx[i + 1];
					stVecDltIndx[i + 1] = t;
				}
			}
		for (i = 0; i < k; i++)
		{
			IMP_VFD_TK_delete_tar(&m_hPara->trackpara, stVecDltIndx[i]);
			m_hPara->trackpara.u8TkNum--;
		}

		IMP_VFD_TK_cross_tar(m_hPara->trackpara.u8TkNum, m_hPara->trackpara.stVecTarget);

		k = 0;
		for (i = 0; i <m_hPara->trackpara.u8TkNum; i++)
		{
			if (m_hPara->trackpara.stVecTarget[i].s32Cred < -15
				|| (m_hPara->trackpara.stVecTarget[i].stROI.s16X2)>pstImage->s32W
				|| (m_hPara->trackpara.stVecTarget[i].stROI.s16Y2)>pstImage->s32H
				|| m_hPara->trackpara.stVecTarget[i].stROI.s16X1 <= 0
				|| m_hPara->trackpara.stVecTarget[i].stROI.s16Y1 <= 0)

			{
				stVecDelete[k++] = i;
			}
		}
		for (i = 0; i < k; i++)
		{
			stVecDltIndx[i] = m_hPara->trackpara.stVecTarget[stVecDelete[i]].u8Indx;
		}
		for (j = 0; j < k; j++)
			for (i = 0; i < k - 1 - j; i++)
			{
				if (stVecDltIndx[i] < stVecDltIndx[i + 1])
				{
					t = stVecDltIndx[i];
					stVecDltIndx[i] = stVecDltIndx[i + 1];
					stVecDltIndx[i + 1] = t;
				}
			}
		for (i = 0; i < k; i++)
		{
			IMP_VFD_TK_delete_tar(&m_hPara->trackpara, stVecDltIndx[i]);
			m_hPara->trackpara.u8TkNum--;
		}

		if (m_hPara->fr % m_Para->u32DetectFQ == 0)
		{
			for (i = 0; i < m_hPara->trackpara.u8TkNum; i++)
			{
				m_hPara->trackpara.stVecTarget[i].u8Dtcflg = 0;
			}

			IMP_VFD_TK_cmp(&m_hPara->trackpara);


			/**/
			if (m_hPara->trackpara.u8VecRoiIndx[1] = 1)
			{
				for (i = 0; i < m_hPara->trackpara.u8VecRoiIndx[3]; i++)
				{
					if (m_hPara->trackpara.u8TkNum < MAXNDETECTIONS)
					if (m_hPara->trackpara.u8TkNum < m_Para->u32DetectNumax)
					{
						m_hPara->trackpara.u8TkIndx = m_hPara->trackpara.u8TkNum;
						m_hPara->trackpara.u8DtcIndx = m_hPara->trackpara.u8VecRoiIndx[i + 4];
						IMP_VFD_TK_Gray_add_tar(&m_hPara->trackpara);
						m_hPara->trackpara.u8TkNum++;
					}
				}
			}

			/**/
			if (m_hPara->trackpara.u8VecRoiIndx[0] = 1)
			{

				for (i = 0; i < m_hPara->trackpara.u8VecRoiIndx[2]; i++)
				{
					if (m_hPara->trackpara.u8TkNum <= MAXNDETECTIONS)
					{
						m_hPara->trackpara.u8TkIndx = i;
						IMP_VFD_TK_init_Gray_ms(&m_hPara->trackpara);
					}
				}
			}


		}
		k = 0;
		j = 0;
		for (i = 0; i < m_hPara->trackpara.u8TkNum; i++)
		{
			if (m_hPara->trackpara.stVecTarget[i].s32Cred < -15
				|| (m_hPara->trackpara.stVecTarget[i].stROI.s16X2)>pstImage->s32W
				|| (m_hPara->trackpara.stVecTarget[i].stROI.s16Y2)>pstImage->s32H
				|| m_hPara->trackpara.stVecTarget[i].stROI.s16X1 <= 0 || m_hPara->trackpara.stVecTarget[i].stROI.s16Y1 <= 0)
			{
				stVecDelete[k++] = i;
			}
		}
		for (i = 0; i < k; i++)
		{
			stVecDltIndx[i] = m_hPara->trackpara.stVecTarget[stVecDelete[i]].u8Indx;
		}
		for (j = 0; j < k; j++)
			for (i = 0; i < k - 1 - j; i++)
			{
				if (stVecDltIndx[i] < stVecDltIndx[i + 1])
				{
					t = stVecDltIndx[i];
					stVecDltIndx[i] = stVecDltIndx[i + 1];
					stVecDltIndx[i + 1] = t;
				}
			}
		for (i = 0; i < k; i++)
		{
			IMP_VFD_TK_delete_tar(&m_hPara->trackpara, stVecDltIndx[i]);
			m_hPara->trackpara.u8TkNum--;
		}


		m_hPara->m_fiOut.s32Facenumber = m_hPara->trackpara.u8TkNum;

		for (i = 0; i < m_hPara->trackpara.u8TkNum; i++)
		{
			m_hPara->m_fiOut.stFace[i].u32FaceID = m_hPara->trackpara.stVecTarget[i].u32ID;
			m_hPara->m_fiOut.stFace[i].stPosition.s16X1 = m_hPara->trackpara.stVecTarget[i].stROI.s16X1;
			m_hPara->m_fiOut.stFace[i].stPosition.s16X2 = m_hPara->trackpara.stVecTarget[i].stROI.s16X2;
			m_hPara->m_fiOut.stFace[i].stPosition.s16Y1 = m_hPara->trackpara.stVecTarget[i].stROI.s16Y1;
			m_hPara->m_fiOut.stFace[i].stPosition.s16Y2 = m_hPara->trackpara.stVecTarget[i].stROI.s16Y2;
			m_hPara->m_fiOut.stFace[i].flagShow = m_hPara->trackpara.stVecTarget[i].u8Showflg;

		}

	}

	return IMP_SUCCESS;
}
