/******************************************************
* FileName: imp_align_tab.h
* Author:  lcy
*
* Description: this file contains alignment model data 
*
*
* Data list:
* 1. NUM_LANDMARK: number of landmarks (face key points)
*
* 2. MEAN_SHAPE�� mean-shape (x1,y1,x2,y2,...)
*    float MEAN_SHAPE[NUM_LANDMARK*2]
*
* 3. NUM_PATTERNS: number of patterns 
*
* 4. DELTAS: list of (delta_x, delta_y)
*    float DELTAS[NUM_PATTERNS*2]
* 5. NUM_CASCADE: number of cascades
*
* 6. NUM_TREE:  number of trees
*
* 7. NUM_SPLIT_NODE: number of split nodes
*
* 8. NUM_LEAF_NODE: number of leaf nodes
*
* 9. NUM_LEFT_EYE_IDX: number of left_eye_index
*
* 10. LEFT_EYE_IDX: left_eye_index in landmarks 
*    (index begin with 0)
*
* 11. NUM_RIGHT_EYE_IDX: number of right_eye_index
*
* 12. RIGHT_EYE_IDX: right_eye_index in landmarks 
*     (index begin with 0)
*
* 13. IDX1: index 1 for split-node, 
*     IDX1[NUM_CASCADE][NUM_TREE][NUM_SPLIT_NODE]
*
* 12. IDX2: index 2 for split-node
*     IDX2[NUM_CASCADE][NUM_TREE][NUM_SPLIT_NODE]
*
* 13. THRES: threshold for split-node
*     THRES[NUM_CASCADE][NUM_TREE][NUM_SPLIT_NODE]
*
* 14. LEAFVAL: leaf values (delta-shape)
*     LEAFVAL[NUM_CASCADE][NUM_TREE][NUM_LEAF_NODE][NUM_LANDMARK][2] 
*
********************************************************************/

#ifndef IMP_ALIGN_TAB_H
#define IMP_ALIGN_TAB_H
#pragma warning (disable:4305)
#pragma warning (disable:4838)


#ifdef __cplusplus
extern "C"
{
#endif

#define MULTIPLY 7 // threshold multiply 2^7=128

#define EYE1 0
#define EYE2 1
#define NOSE 2
#define MOUTH1 3
#define MOUTH2 4

#define EYE1_X 0
#define EYE1_Y 1
#define EYE2_X 2
#define EYE2_Y 3
#define NOSE_X 4
#define NOSE_Y 5
#define MOUTH1_X 6
#define MOUTH1_Y 7
#define MOUTH2_X 8
#define MOUTH2_Y 9

#define NUM_LANDMARK 5
#define NUM2 10
#define NUM_LMinv 0.2
static const float MEAN_SHAPE[NUM_LANDMARK*2] = {
     0.339737833,0.338022977,
    0.68713665,0.330665827,
    0.520080924,0.602009058,
    0.370064348,0.717614233,
    0.66813004,0.711334705};
#define NUM_PATTERNS 43
static const float DELTAS[NUM_PATTERNS*2] = { 
    0.1158256307,0,
    0.05791272596,0.1003078446,
    -0.05791272596,0.1003078446,
    -0.1158256307,0,
    -0.05791272596,-0.1003078446,
    0.05791272596,-0.1003078446,
    0.07523097098,0.0434345901,
    0,0.0868691802,
    -0.07523097098,0.0434345901,
    -0.07523097098,-0.0434345901,
    -0,-0.0868691802,
    0.07523097098,-0.0434345901,
    0.06273882836,0,
    0.03136950359,0.05433337018,
    -0.03136950359,0.05433337018,
    -0.06273882836,0,
    -0.03136950359,-0.05433337018,
    0.03136950359,-0.05433337018,
    0.03761539981,0.02171729505,
    0,0.0434345901,
    -0.03761539981,0.02171729505,
    -0.03761539981,-0.02171729505,
    -0,-0.0434345901,
    0.03761539981,-0.02171729505,
    0.02895645052,0,
    0.01447813865,0.02507704683,
    -0.01447813865,0.02507704683,
    -0.02895645052,0,
    -0.01447813865,-0.02507704683,
    0.01447813865,-0.02507704683,
    0.0167179741,0.009652208537,
    0,0.01930424385,
    -0.0167179741,0.009652208537,
    -0.0167179741,-0.009652208537,
    -0,-0.01930424385,
    0.0167179741,-0.009652208537,
    0.01447813865,0,
    0.007239155937,0.01253852341,
    -0.007239155937,0.01253852341,
    -0.01447813865,0,
    -0.007239155937,-0.01253852341,
    0.007239155937,-0.01253852341,
    -0,0};
#define NUM_CASCADE 12
#define NUM_TREE 100
#define NUM_SPLIT_NODE 31
#define NUM_LEAF_NODE 32
#define NUM_LEFT_EYE_IDX 1
static int LEFT_EYE_IDX[NUM_LEFT_EYE_IDX] = {0};
#define NUM_RIGHT_EYE_IDX 1
static int RIGHT_EYE_IDX[NUM_LEFT_EYE_IDX] = {1};

extern const int IDX1[NUM_CASCADE][NUM_TREE][NUM_SPLIT_NODE]; 
extern const int IDX2[NUM_CASCADE][NUM_TREE][NUM_SPLIT_NODE];
extern const int THRES[NUM_CASCADE][NUM_TREE][NUM_SPLIT_NODE] ;
extern const float LEAFVAL[NUM_CASCADE][NUM_TREE][NUM_LEAF_NODE][NUM_LANDMARK][2];


#ifdef __cplusplus
}
#endif

#endif  /*IMP_ALIGN_TAB_H*/

