#include "imp_vfd_detect.h"
#include<stdio.h>
#include "imp_vfd_api.h"
#include"imp_vfd_track.h"

#define _MAX_2(a, b) ((a)>(b)?(a):(b))
#define _MIN_2(a, b) ((a)<(b)?(a):(b))

IMP_HANDLE IMP_VFD_Create(IMP_S32 s32Width, IMP_S32 s32Height)
{
	if (s32Width <= 0 || s32Height <= 0)
	{
		printf("width or height less than 0 \n");
		return IMP_NULL;
	}
	IMP_S32 track_sucess;

	IMP_U32 size;
	IMP_HANDLE *m_hArr = (IMP_HANDLE*)malloc(sizeof(IMP_HANDLE) * 2);
	if (m_hArr == NULL)
	{
		return IMP_NULL;
	}

	m_hArr[0] = (VFD_MODULE_S*)malloc(sizeof(VFD_MODULE_S));
	memset(m_hArr[0], 0, sizeof(VFD_MODULE_S));

	if (m_hArr[0] == NULL)
	{
		//printf("Not enough memory to malloc\n");
		free(m_hArr);
		return IMP_NULL;
	}


	m_hArr[1] = (IMP_VFD_PARA_S *)malloc(sizeof(IMP_VFD_PARA_S));
	memset(m_hArr[1], 0, sizeof(IMP_VFD_PARA_S));

	if (m_hArr[1] == NULL)
	{
		free(m_hArr[0]);
		free(m_hArr);
		return IMP_NULL;
	}

	VFD_MODULE_S * temp = (VFD_MODULE_S *)m_hArr[0];

	if (s32Height <= 0 || s32Width <= 0)
		return IMP_NULL;

	temp->maxHeight = s32Height;
	temp->maxWidth = s32Width;
	size = s32Height * s32Width;
	temp->pruningMask = (IMP_GRAY_IMAGE_S *)malloc(sizeof(IMP_GRAY_IMAGE_S));

	if (temp->pruningMask == NULL)
	{
		free(m_hArr[1]);
		free(m_hArr[0]);
		free(m_hArr);
		return IMP_NULL;
	}
	//if ()
	temp->pruningMask->s32H = s32Height;
	temp->pruningMask->s32W = s32Width;
	temp->pruningMask->pu8Data = (IMP_U8 *)malloc((size)*sizeof(IMP_U8));
	if (temp->pruningMask->pu8Data == NULL)
	{
		//printf("Not enough memory to malloc\n");
		free(temp->pruningMask);
		free(m_hArr[1]);
		free(m_hArr[0]);
		free(m_hArr);
		return IMP_NULL;
	}
	memset(temp->pruningMask->pu8Data, 0, size * sizeof(IMP_U8));
	temp->pruningBuff = (SAT_BUFF_S *)malloc(sizeof(SAT_BUFF_S));
	temp->pruningBuff->u32Sum = (sumtype *)malloc((s32Width + 1) * (s32Height + 1)*sizeof(sumtype));
	if (temp->pruningBuff->u32Sum == NULL)
	{
		//printf("Not enough memory to malloc\n");
		free(temp->pruningMask->pu8Data);
		free(temp->pruningMask);
		free(m_hArr[1]);
		free(m_hArr[0]);

		return IMP_NULL;
	}
	memset(temp->pruningBuff->u32Sum, 0, sizeof(IMP_S32)* (s32Width + 1) * (s32Height + 1));
	temp->pruningBuff->s32P = s32Width;
	temp->pruningBuff->s32SP = s32Width + 1;
	temp->pruningBuff->s32W = s32Width;
	temp->pruningBuff->s32H = s32Height;

	temp->picopara.facecenterX = (IMP_S32 *)malloc(INNERMAXDETECT*sizeof(IMP_S32));
	temp->picopara.facecenterY = (IMP_S32 *)malloc(INNERMAXDETECT*sizeof(IMP_S32));
	temp->picopara.faceradius = (IMP_S32 *)malloc(INNERMAXDETECT*sizeof(IMP_S32));
	temp->picopara.facethreshold = (IMP_S32 *)malloc(INNERMAXDETECT*sizeof(IMP_S32));
	temp->picopara.rotateangle = (float *)malloc(INNERMAXDETECT*sizeof(float));

	// faceQA_create

	temp->QAlevel = 2;

	IMP_VFD_TK_create(&temp->trackpara, s32Height, s32Width);

	return m_hArr;
}
IMP_U32 IMP_IsSkin_Color(IMP_U8 u, IMP_U8 v)
{
	/*return (v>=148 && v<=185 &&(u + 0.6*v)>=189 && (u+0.6*v)<=215);*/
	if (u <= 135 && u >= 85 && v <= 177 && v >= 133)
		return 1;
	else
		return 0;
	/*return (u<=125&& u>=75&& v<=173 && v>=133);*/
}

void IMP_calculate_Buff(const IMP_GRAY_IMAGE_S* m_pSrc, SAT_BUFF_S *m_sDst)
{
	/*store_sq=IMP_TRUE;*/
	int sumStep = m_pSrc->s32W + 1;
	int srcStep = m_pSrc->s32W;
	unsigned int H = m_pSrc->s32H, W = m_pSrc->s32W;
	IMP_U8* src = m_pSrc->pu8Data;
	sumtype s;
	register unsigned int y, x;


	sumtype* sum = m_sDst->u32Sum - sumStep;
	sum += sumStep + 1;

	for (y = 0; y < H; y++, src += srcStep, sum += sumStep)
	{
		sum[-1] = 0;
		for (x = 0, s = 0; x < W; x++)
		{
			sumtype t = (sumtype)src[x];
			s += t;

			sum[x + sumStep] = sum[x] + s;

		}
	}

}

void IMP_Color_Segmentation(VFD_MODULE_S *m_hPara, const IMAGE3_S* src_frame)
{
	int i, j;
	int width = src_frame->s32W;
	int height = src_frame->s32H;
	int imwidth = (width >> 1);
	IMP_U8 *U = src_frame->pu8D2;
	IMP_U8 *V = src_frame->pu8D3;

	memset(m_hPara->pruningMask->pu8Data, 0, sizeof(IMP_U8)*width*height);
	/*int imheight = (height>>1);*/


	for (j = 0; j<height - 2; j += 2)
	{
		for (i = 0; i<width - 2; i += 2)
		{
			if (IMP_IsSkin_Color(U[(i >> 1) + (j >> 1)*imwidth], V[(i >> 1) + (j >> 1)*imwidth]))
			{
				m_hPara->pruningMask->pu8Data[i + j * width] = 1;
				m_hPara->pruningMask->pu8Data[i + 1 + j*width] = 1;
				m_hPara->pruningMask->pu8Data[i + (j + 1)*width] = 1;
				m_hPara->pruningMask->pu8Data[i + 1 + (j + 1)*width] = 1;
			}
		}
	}

	IMP_calculate_Buff(m_hPara->pruningMask, m_hPara->pruningBuff);
}



IMP_U32 IMP_VFD_Config(IMP_HANDLE hModule, IMP_VFD_PARA_S *pstVFDParaSrc)
{
	int i;
	if (!hModule)
	{
		printf("Handle NULL\n");
		return IMP_NULL;
	}


	IMP_HANDLE *m_ptr = (IMP_HANDLE *)hModule;
	VFD_MODULE_S *m_AlgoPara = (VFD_MODULE_S *)m_ptr[0];
	IMP_VFD_PARA_S *m_Para = (IMP_VFD_PARA_S *)m_ptr[1];


	memcpy((IMP_VFD_PARA_S *)(m_ptr[1]), pstVFDParaSrc, sizeof(IMP_VFD_PARA_S));


	/* faceQAlevel */
	if (m_Para->QAlevel > 0 && m_Para->QAlevel < 5)
		m_AlgoPara->QAlevel = m_Para->QAlevel;
	else
	{
		//printf("beyond QAlevel limit, we set it to default Level 2\n");
		m_AlgoPara->u32FDTimes = 7878;
		m_AlgoPara->QAlevel = 2;
	}

	if (m_Para->s32Sizemin < 0 || m_Para->s32Sizemax < 0
		|| m_Para->s32Sizemin > _MIN_2(m_AlgoPara->maxHeight, m_AlgoPara->maxWidth)
		|| m_Para->s32Sizemax > _MAX_2(m_AlgoPara->maxHeight, m_AlgoPara->maxWidth)
		)
	{
		//printf("Minsize or Maxsize <0\n");
		m_AlgoPara->u32FDTimes = 7878;
		return IMP_FALSE;
	}

	if (m_Para->stROI.s32Enable < 0 || m_Para->stROI.s32Enable>1)
	{
		m_AlgoPara->u32FDTimes = 7878;
		m_Para->stROI.s32Enable = 0;
	}

	if (m_Para->u32EnableColor < 0 || m_Para->u32EnableColor > 1)
	{
		m_AlgoPara->u32FDTimes = 7878;
		m_Para->u32EnableColor = 0;
	}

	if (m_Para->u32EnableGrayTrack < 0 || m_Para->u32EnableGrayTrack > 1)
	{
		m_AlgoPara->u32FDTimes = 7878;
		m_Para->u32EnableGrayTrack = 0;
	}

	if (m_Para->u32EnableQA < 0 || m_Para->u32EnableQA > 1)
	{
		m_AlgoPara->u32FDTimes = 7878;
		m_Para->u32EnableQA = 0;
	}

	if (m_Para->u32Enablerotate < 0 || m_Para->u32Enablerotate > 1)
	{
		m_AlgoPara->u32FDTimes = 7878;
		m_Para->u32Enablerotate = 0;
	}


	if (m_Para->u32EnableTrack < 0 || m_Para->u32EnableTrack > 1)
	{
		m_AlgoPara->u32FDTimes = 7878;
		m_Para->u32EnableTrack = 0;
	}


	if (m_Para->u32RotateLevel < 0 && m_Para->u32RotateLevel> 4)
	{
		m_AlgoPara->u32FDTimes = 7878;
		m_Para->u32RotateLevel = 1;
	}

	if (m_Para->stROI.s32Enable == 1)
	{
		if (m_Para->stROI.stPolygon.u32VertexNum != 4 ||
			(m_Para->stROI.stPolygon.astVertex[1].s16Y != m_Para->stROI.stPolygon.astVertex[0].s16Y
			&& m_Para->stROI.stPolygon.astVertex[1].s16Y != m_Para->stROI.stPolygon.astVertex[2].s16Y) ||
			(m_Para->stROI.stPolygon.astVertex[1].s16X != m_Para->stROI.stPolygon.astVertex[2].s16X
			&& m_Para->stROI.stPolygon.astVertex[1].s16X != m_Para->stROI.stPolygon.astVertex[0].s16X)
			|| (m_Para->stROI.stPolygon.astVertex[3].s16Y != m_Para->stROI.stPolygon.astVertex[2].s16Y
			&& m_Para->stROI.stPolygon.astVertex[3].s16Y != m_Para->stROI.stPolygon.astVertex[0].s16Y)
			|| (m_Para->stROI.stPolygon.astVertex[3].s16X != m_Para->stROI.stPolygon.astVertex[0].s16X
			&& m_Para->stROI.stPolygon.astVertex[3].s16X != m_Para->stROI.stPolygon.astVertex[2].s16X))
		{
			//printf("ROI region only support rectangle!\n");
			m_AlgoPara->u32FDTimes = 7878;
			return IMP_FALSE;
		}

		if (m_Para->stROI.stPolygon.u32VertexNum == 4)
		{
			if (m_Para->stROI.stPolygon.astVertex[0].s16X<0
				|| m_Para->stROI.stPolygon.astVertex[0].s16Y<0
				|| m_Para->stROI.stPolygon.astVertex[0].s16X >m_AlgoPara->maxWidth
				|| m_Para->stROI.stPolygon.astVertex[0].s16Y>m_AlgoPara->maxHeight
				|| m_Para->stROI.stPolygon.astVertex[2].s16X<0
				|| m_Para->stROI.stPolygon.astVertex[2].s16Y<0
				|| m_Para->stROI.stPolygon.astVertex[2].s16X >m_AlgoPara->maxWidth
				|| m_Para->stROI.stPolygon.astVertex[2].s16Y>m_AlgoPara->maxHeight
				)
			{
				//printf("error in set ROI area\n");
				m_AlgoPara->u32FDTimes = 7878;
				return IMP_FALSE;
			}

		}
	}


	if (m_Para->u32DetectFQ < 1)
	{
		//printf("detect frequence set to 1!\n");
		m_AlgoPara->u32FDTimes = 7878;
		m_Para->u32DetectFQ = 1;
	}

	if (m_Para->u32DetectFQ > 10000){
		//printf("detect frequence set to 1!\n");
		m_AlgoPara->u32FDTimes = 7878;
		m_Para->u32DetectFQ = 1;
	}


	if (m_Para->s32Sizemin > m_Para->s32Sizemax)
	{
		m_AlgoPara->u32FDTimes = 7878;
		//printf("minszize > maxsize, reset it\n");
		return IMP_FALSE;
	}

	m_AlgoPara->picopara.faceminsize = m_Para->s32Sizemin;
	m_AlgoPara->picopara.facemaxsize = m_Para->s32Sizemax;

	m_AlgoPara->picopara.stridefactor = 0.1;


	if (m_Para->s32DetectLevel >= 0 && m_Para->s32DetectLevel <= 5)
	{
		m_AlgoPara->picopara.scalefactor = fTbs[m_Para->s32DetectLevel];
	}
	else
	{
		//printf("beyond DetectLevel limit, we set it to default Level 2\n");
		m_AlgoPara->u32FDTimes = 7878;
		m_AlgoPara->picopara.scalefactor = 1.1;
	}

	/*---------Track Config---------*/


	if (m_Para->s32TrackLevel >= 1 && m_Para->s32TrackLevel <= 5)
	{
		m_AlgoPara->trackpara.u16IteraNum = tTbs[m_Para->s32TrackLevel];
	}
	else
	{
		m_AlgoPara->u32FDTimes = 7878;
		//printf("beyond TrackLevel limit, we set it to default Level 1\n");
		m_AlgoPara->trackpara.u16IteraNum = 5;
	}
	if (m_Para->s32Confidence >= 0 && m_Para->s32Confidence <= 5)
	{
		m_AlgoPara->trackpara.u8Confds = cTbs[m_Para->s32Confidence];
	}
	else
	{
		m_AlgoPara->u32FDTimes = 7878;
		//printf("beyond Confidence limit, we set it to default 1\n");
		m_AlgoPara->trackpara.u8Confds = 1;
	}
	if (m_Para->s32Sense >= 0 && m_Para->s32Sense <= 5)
	{
		m_AlgoPara->trackpara.u8Sense = sTbs[m_Para->s32Sense];
	}
	else
	{
		m_AlgoPara->u32FDTimes = 7878;
		//printf("beyond Sense limit, we set it to default 1\n");
		m_AlgoPara->trackpara.u8Sense = 1;
	}

	if (m_Para->u8DetectAccuracy >= 0 && m_Para->u8DetectAccuracy <= 5)
	{
		m_AlgoPara->picothreshold = threshTbs[m_Para->u8DetectAccuracy];
	}
	else
	{
		m_AlgoPara->u32FDTimes = 7878;
		//printf("beyond DetectAccuracy limit, we set it to default level 2\n");
		m_AlgoPara->picothreshold = 5;
	}


	if (m_Para->u32DetectNumax > IMP_VFD_MAX_FACE_NUMBER || m_Para->u32DetectNumax< 0)
	{
		m_AlgoPara->u32FDTimes = 7878;
		m_Para->u32DetectNumax = IMP_VFD_MAX_FACE_NUMBER;
		//printf("beyond DetectNumber limit, we set it to dafault %d\n", m_Para->u32DetectNumax);
	}


	return IMP_SUCCESS;
}




IMP_S32 IMP_VFD_overlap(int x11, int y11, int x12, int y12, int x21, int y21, int x22, int y22)
{
	IMP_S16 overr, overc;
	IMP_S16 r1, r2;

	r1 = _MAX_2(y12 - y11, x12 - x11);
	r2 = _MAX_2(y22 - y21, x22 - x21);

	if (r1 <= 0 || r2 <= 0)
	{
		return 0;
	}

	overr = _MAX_2(0, _MIN_2(x12, x22) - _MAX_2(x11, x21));
	overc = _MAX_2(0, _MIN_2(y12, y22) - _MAX_2(y11, y21));

	return _MIN_2(((overr*overc) << 7) / (_MIN_2(r1, r2)*_MIN_2(r1, r2)), 128);
}


/*
					nms
		kill small ones in the biggest
*/
void IMP_NMS_Inside(VFD_MODULE_S *m_hPara, IMP_VFD_PARA_S *m_Para)

{
	IMP_S32 nrof_faces, smallimg_flag, real_num;
	IMP_U8 small_img_index[MAXNDETECTIONS] = { 0 };
	int i, j, k;

	nrof_faces = 0;
	for (i = 0; i< j; i++)
		for (k = i + 1; k<j; k++)
		{
			if (IMP_VFD_overlap(
				m_hPara->m_fiOut.stFace[i].stPosition.s16X1, m_hPara->m_fiOut.stFace[i].stPosition.s16Y1,
				m_hPara->m_fiOut.stFace[i].stPosition.s16X2, m_hPara->m_fiOut.stFace[i].stPosition.s16Y2,
				m_hPara->m_fiOut.stFace[k].stPosition.s16X1, m_hPara->m_fiOut.stFace[k].stPosition.s16Y1,
				m_hPara->m_fiOut.stFace[k].stPosition.s16X2, m_hPara->m_fiOut.stFace[k].stPosition.s16Y2)>80)

				if (((m_hPara->m_fiOut.stFace[i].stPosition.s16X2 - m_hPara->m_fiOut.stFace[i].stPosition.s16X1)*
					(m_hPara->m_fiOut.stFace[i].stPosition.s16X2 - m_hPara->m_fiOut.stFace[i].stPosition.s16X1))
					<((m_hPara->m_fiOut.stFace[k].stPosition.s16X2 - m_hPara->m_fiOut.stFace[k].stPosition.s16X1)*
					(m_hPara->m_fiOut.stFace[k].stPosition.s16X2 - m_hPara->m_fiOut.stFace[k].stPosition.s16X1))
					)
				{
					small_img_index[nrof_faces++] = i;
				}
				else
				{
					small_img_index[nrof_faces++] = k;
				}
		}

	smallimg_flag = 0;
	real_num = 0;
	for (i = 0; i< m_hPara->m_fiOut.s32Facenumber; i++)
	{
		for (j = 0; j< nrof_faces; j++)
		{
			if (i == small_img_index[j])
				smallimg_flag = 1;
		}
		if (!smallimg_flag)
		{
			m_hPara->m_fiOut.stFace[real_num].stPosition.s16X1 = m_hPara->m_fiOut.stFace[i].stPosition.s16X1;
			m_hPara->m_fiOut.stFace[real_num].stPosition.s16X2 = m_hPara->m_fiOut.stFace[i].stPosition.s16X2;
			m_hPara->m_fiOut.stFace[real_num].stPosition.s16Y1 = m_hPara->m_fiOut.stFace[i].stPosition.s16Y1;
			m_hPara->m_fiOut.stFace[real_num].stPosition.s16Y2 = m_hPara->m_fiOut.stFace[i].stPosition.s16Y2;
			m_hPara->m_fiOut.stFace[real_num].angle = m_hPara->m_fiOut.stFace[i].angle;
			if (!m_Para->u32EnableTrack)
			{
				m_hPara->m_fiOut.stFace[real_num].flagShow = 1;
			}
			real_num++;
		}
	}
	m_hPara->m_fiOut.s32Facenumber = real_num;

}

IMP_U32 IMP_VFD_Process(IMP_HANDLE hModule, IMAGE3_S *pstImage)
{
	if (!hModule || !pstImage || !pstImage->pu8D1)
	{
		//printf("Handle or psrImage or pu8D1 NULL\n");
		return IMP_FAILURE;
	}


	static IMP_U32 time = 0;
	static int flag = 0;
	static int fr = 0;
	int temp_nd, temp_maxface, temp_minface;
	int pts;
	int nrof_faces, smallimg_flag, real_num;
	IMP_U8 small_img_index[MAXNDETECTIONS] = { 0 };
	int resize;

	IMP_HANDLE *m_ptr = (IMP_HANDLE *)hModule;
	VFD_MODULE_S *m_hPara = (VFD_MODULE_S*)m_ptr[0];
	IMP_VFD_PARA_S *m_Para = (IMP_VFD_PARA_S *)m_ptr[1];

	IMP_S32 i, j;

	if (pstImage->s32H != m_hPara->maxHeight
		|| pstImage->s32W != m_hPara->maxWidth
		|| m_hPara->u32FDTimes == 7878)
	{
		//printf("Handle or psrImage or pu8D1 NULL\n");
		return IMP_FAILURE;
	}

	time++;
	if (time > VFD_TIME_LIMIT)
	{
		return IMP_FAILURE;
	}

	fr++;
	if (fr < 0)
	{
		fr = 0;
	}

	m_hPara->fr = fr;


	if (fr %  m_Para->u32DetectFQ == 0)
	{
		if (m_Para->u32EnableColor)
		{
			IMP_Color_Segmentation(m_hPara, pstImage);
			IMP_Color_findobjects(&m_hPara->picopara, pstImage, m_hPara->pruningBuff->u32Sum, m_Para);
		}

		else
		{
			IMP_Gray_findobjects(&m_hPara->picopara, pstImage, m_Para);
		}

		cluster_detections(&m_hPara->picopara, m_Para->u32DetectNumax);
		m_hPara->m_fiOut.s32Facenumber = 0;


		j = 0;
		for (i = 0; i<m_hPara->picopara.ndetections; i++)
		{

			if (m_hPara->picopara.facethreshold[i] >= m_hPara->picothreshold)
			{

				m_hPara->m_fiOut.stFace[j].stPosition.s16X1 = m_hPara->picopara.facecenterX[i] - m_hPara->picopara.faceradius[i] / 2;
				m_hPara->m_fiOut.stFace[j].stPosition.s16X2 = m_hPara->picopara.facecenterX[i] + m_hPara->picopara.faceradius[i] / 2;
				m_hPara->m_fiOut.stFace[j].stPosition.s16Y1 = m_hPara->picopara.facecenterY[i] - m_hPara->picopara.faceradius[i] / 2;
				m_hPara->m_fiOut.stFace[j].stPosition.s16Y2 = m_hPara->picopara.facecenterY[i] + m_hPara->picopara.faceradius[i] / 2;
				/*boundary protection*/
				if (m_hPara->m_fiOut.stFace[j].stPosition.s16X1 > 0 && m_hPara->m_fiOut.stFace[j].stPosition.s16X2 < pstImage->s32W
					&& m_hPara->m_fiOut.stFace[j].stPosition.s16Y1>0 && m_hPara->m_fiOut.stFace[j].stPosition.s16Y2 < pstImage->s32H)
				{
					m_hPara->m_fiOut.s32Facenumber++;
					m_hPara->m_fiOut.stFace[j].angle = m_hPara->picopara.rotateangle[i];

					j++;


				}
			}
		}

		IMP_NMS_Inside(m_hPara, m_Para);


	}

	if (m_Para->u32EnableTrack)
	{
		m_hPara->trackpara.u8VecRoiIndx[0] = 0;
		m_hPara->trackpara.u8VecRoiIndx[1] = 0;
		if (fr % m_Para->u32DetectFQ == 0)
		{
			memset(&m_hPara->trackpara.s32VecDtc, 0, sizeof(MAXNDETECTIONS * 4 + 1));
			m_hPara->trackpara.s32VecDtc[0] = m_hPara->m_fiOut.s32Facenumber;

			for (i = 0; i < m_hPara->trackpara.s32VecDtc[0]; i++)
			{
				flag = 1;
				m_hPara->trackpara.s32VecDtc[4 * i + 1] = m_hPara->m_fiOut.stFace[i].stPosition.s16X1;
				m_hPara->trackpara.s32VecDtc[4 * i + 2] = m_hPara->m_fiOut.stFace[i].stPosition.s16Y1;
				m_hPara->trackpara.s32VecDtc[4 * i + 3] = m_hPara->m_fiOut.stFace[i].stPosition.s16X2;
				m_hPara->trackpara.s32VecDtc[4 * i + 4] = m_hPara->m_fiOut.stFace[i].stPosition.s16Y2;

			}
		}

		if (m_Para->u32EnableGrayTrack)
		{
			if (flag)
			{
				IMP_VFD_TK_gray(hModule, pstImage);
			}
		}

		else
		{
			if (flag)
			{
				IMP_VFD_TK_color(hModule, pstImage);
			}
		}

	}

	if (m_Para->u32EnableQA)
	{
		if (m_Para->u32EnableTrack)
		{
			for (i = 0; i < m_hPara->trackpara.u8TkNum; i++)
			{
				m_hPara->m_fiOut.stFace[i].alignRes.align_success = -1;
				for (pts = 0; pts < NUM_LANDMARK; pts++) {
					m_hPara->m_fiOut.stFace[i].alignRes.f32Shape[pts].f32X = 0.f;
					m_hPara->m_fiOut.stFace[i].alignRes.f32Shape[pts].f32Y = 0.f;

				}
				m_hPara->m_fiOut.stFace[i].faceQA_Res = -1;


				if (fr %  m_Para->u32DetectFQ == 0)
				{
					m_hPara->m_fiOut.stFace[i].faceQA_Res = -1;
					/*if detected :*/
					if (m_hPara->trackpara.stVecTarget[i].u8Dtcflg == 1)
					{
						m_hPara->m_fiOut.stFace[i].faceQA_Res = evaluate_faceQA(pstImage, &m_hPara->m_fiOut.stFace[i].stPosition,
							&m_hPara->m_fiOut.stFace[i].alignRes, m_hPara->QAlevel);
					}
				}

			}
		}
		else
		{
			for (i = 0; i < m_hPara->m_fiOut.s32Facenumber; i++)
			{
				m_hPara->m_fiOut.stFace[i].alignRes.align_success = -1;
				for (pts = 0; pts < NUM_LANDMARK; pts++) {
					m_hPara->m_fiOut.stFace[i].alignRes.f32Shape[pts].f32X = 0.f;
					m_hPara->m_fiOut.stFace[i].alignRes.f32Shape[pts].f32Y = 0.f;
				}
				m_hPara->m_fiOut.stFace[i].faceQA_Res = -1;


				if (fr %  m_Para->u32DetectFQ == 0)
				{
					m_hPara->m_fiOut.stFace[i].faceQA_Res = -1;
					/*if detected :*/

					m_hPara->m_fiOut.stFace[i].faceQA_Res = evaluate_faceQA(pstImage, &m_hPara->m_fiOut.stFace[i].stPosition,
						&m_hPara->m_fiOut.stFace[i].alignRes, m_hPara->QAlevel);
				}

			}

		}
	}
	return IMP_SUCCESS;
}


IMP_U32 IMP_VFD_GetResult(IMP_HANDLE hModule, IMP_VFD_RESULT_S *pstVFDResult)
{
	if (!hModule)
	{
		printf("Handle NULL\n");
		return IMP_FAILURE;
	}

	IMP_HANDLE *m_ptr = (IMP_HANDLE *)hModule;

	VFD_MODULE_S * m_AlgoPara = (VFD_MODULE_S*)m_ptr[0];
	IMP_VFD_PARA_S *m_Para = (IMP_VFD_PARA_S *)m_ptr[1];
	int i, j;


	memset(pstVFDResult, 0, sizeof(IMP_VFD_RESULT_S));

	pstVFDResult->s32Facenumber = m_AlgoPara->m_fiOut.s32Facenumber;
	for (i = 0; i < m_AlgoPara->m_fiOut.s32Facenumber; i++)
	{
		pstVFDResult->stFace[i].u32FaceID = m_AlgoPara->m_fiOut.stFace[i].u32FaceID;
		pstVFDResult->stFace[i].stPosition.s16X1 = m_AlgoPara->m_fiOut.stFace[i].stPosition.s16X1;
		pstVFDResult->stFace[i].stPosition.s16X2 = m_AlgoPara->m_fiOut.stFace[i].stPosition.s16X2;
		pstVFDResult->stFace[i].stPosition.s16Y1 = m_AlgoPara->m_fiOut.stFace[i].stPosition.s16Y1;
		pstVFDResult->stFace[i].stPosition.s16Y2 = m_AlgoPara->m_fiOut.stFace[i].stPosition.s16Y2;
		pstVFDResult->stFace[i].u8Flag = m_AlgoPara->m_fiOut.stFace[i].flagShow;



		pstVFDResult->stFace[i].faceQA_Res = m_AlgoPara->m_fiOut.stFace[i].faceQA_Res;

	}

	return IMP_SUCCESS;
}

IMP_U32 IMP_VFD_Release(IMP_HANDLE* pHandle)
{

	if (!(*pHandle))
	{
		printf("Handle NULL\n");
		return IMP_FAILURE;
	}

	IMP_HANDLE *m_ptr = (IMP_HANDLE *)(*pHandle);
	VFD_MODULE_S * pstVFDModule = (VFD_MODULE_S *)m_ptr[0];

	free((IMP_VFD_PARA_S *)m_ptr[1]); m_ptr[1] = NULL;

	free(pstVFDModule->pruningMask->pu8Data);
	free(pstVFDModule->pruningMask);
	free(pstVFDModule->pruningBuff->u32Sum);
	free(pstVFDModule->pruningBuff);
	free(pstVFDModule->picopara.facecenterX);
	free(pstVFDModule->picopara.facecenterY);
	free(pstVFDModule->picopara.faceradius);
	free(pstVFDModule->picopara.facethreshold);
	free(pstVFDModule->picopara.rotateangle);

	IMP_VFD_TK_release(&pstVFDModule->trackpara);

	free((VFD_MODULE_S *)m_ptr[0]);
	m_ptr[0] = NULL;

	free(m_ptr); m_ptr = NULL;
	*pHandle = NULL;


	return IMP_SUCCESS;
}
