#include <stdio.h>
#include<string.h>
#include"imp_faceQA.h"
#include "imp_vfd_api.h"

#include "Util\utils.h"



void vfd_draw(IMP_VFD_RESULT_S stResult, IMAGE3_S *frame) {
	int num_object, i;
	static int fr = 1;
	unsigned char color[3] = { 0xff, 0x80, 0x80 };
	char ID_str[20];


	if (stResult.s32Facenumber > 0)
	{
		num_object = stResult.s32Facenumber;
		for (i = 0; i < num_object; i++) {
			//if (stResult.stFace[i].u8Flag == 1){
				// rect 
				memset(ID_str, 0, 20);

				IMP_Draw_Rect(frame, stResult.stFace[i].stPosition, 2, color);
				//strcpy(ID_str, "ID:");

				printf("faceID:%d (%d, %d, %d, %d)\n", stResult.stFace[i].u32FaceID,
					stResult.stFace[i].stPosition.s16X1, stResult.stFace[i].stPosition.s16Y1,
					stResult.stFace[i].stPosition.s16X2 - stResult.stFace[i].stPosition.s16X1,
					stResult.stFace[i].stPosition.s16Y2 - stResult.stFace[i].stPosition.s16Y1);
				// alignment
				_itoa(stResult.stFace[i].u32FaceID, ID_str, 10);

				printf("QA: %d\n", stResult.stFace[i].faceQA_Res);
		}
	}
	fr++;
}

int main(int argc, char** argv)
{
	// ================================================
	//               yuv input and output
	//=================================================
	int inwidth = 362;
	int inheight = 310;
	int size = inwidth*inheight;
	IMAGE3_S *frame = NULL;
	frame = (IMAGE3_S *)malloc(sizeof(IMAGE3_S));
	frame->s32H = inheight;
	frame->s32W = inwidth;
	frame->pu8D1 = (IMP_U8 *)malloc((size * 3 / 2) * sizeof(IMP_U8));
	memset(frame->pu8D1, 0, size * sizeof(IMP_U8));
	frame->pu8D2 = frame->pu8D1 + size;
	frame->pu8D3 = frame->pu8D2 + size / 4;
	char* buffer;

	FILE *yuvFile, *resultFile;
	yuvFile = fopen("D:/xj/qqfile/1340990584/FileRecv/20.yuv", "rb");

	int u32Ret;
	if (NULL == yuvFile)
	{
		printf("Can not open file vfd.yuv\n");
		return 1;
	}
	resultFile = fopen("D:/xj/qqfile/1340990584/FileRecv/20_362x310.yuv", "w");
	if (NULL == yuvFile)
	{
		printf("Can not open file result.yuv\n");
		return 1;
	}
	buffer = (char *)malloc(size  * sizeof(char));

	// =============================================
	//                    VFD Create
	// ==============================================
	IMP_HANDLE hDHandle = NULL;
	hDHandle = IMP_VFD_Create(inwidth, inheight);
	// =============================================
	//                    VFD Config
	// ==============================================
	/* parameter */
	IMP_VFD_PARA_S m_para;
	m_para.s32DetectLevel = 2;
	m_para.u32DetectFQ = 1;/*detect frequence*/
	m_para.u32DetectNumax = 3;//1: find biggest objects
	m_para.s32Sizemax = 200;
	m_para.s32Sizemin = 40;
	m_para.u8DetectAccuracy = 2;

	m_para.u32EnableColor = 0;
	m_para.u32Enablerotate = 0;
	m_para.u32RotateLevel = 4;

	m_para.stROI.s32Enable = 1;
	m_para.stROI.stPolygon.u32VertexNum = 4;
	m_para.stROI.stPolygon.astVertex[0].s16X = 0;
	m_para.stROI.stPolygon.astVertex[0].s16Y = 0;

	m_para.stROI.stPolygon.astVertex[1].s16X = inwidth;
	m_para.stROI.stPolygon.astVertex[1].s16Y = 0;

	m_para.stROI.stPolygon.astVertex[2].s16X = inwidth;
	m_para.stROI.stPolygon.astVertex[2].s16Y = inheight;

	m_para.stROI.stPolygon.astVertex[3].s16X = 0;
	m_para.stROI.stPolygon.astVertex[3].s16Y = inheight;


	m_para.u32EnableQA = 1;
	m_para.QAlevel = 2;
	/* track */
	m_para.u32EnableTrack = 0;
	m_para.u32EnableGrayTrack = 1;
	m_para.s32TrackLevel = 1;
	m_para.s32Sense = 3;
	m_para.s32Confidence = 1;
	//config
	IMP_VFD_Config(hDHandle, &m_para);

	// =============================================
	//                    VFD Result
	// ==============================================
	IMP_VFD_RESULT_S stResult;


	int total_face = 0;
	int fr = 0;
	int n = 0;

	/*read yuv_420 frame by frame*/
	for (;;)
	{
		u32Ret = fread(buffer, size, 1, yuvFile);
		if (1 != u32Ret)
		{
			printf("YUVfile end\n");
			break;
		}
		memcpy(frame->pu8D1, buffer, size);

		u32Ret = fread(buffer, size / 2, 1, yuvFile);
		if (1 != u32Ret)
		{
			printf("YUVfile end\n");
			break;
		}
		memcpy(frame->pu8D2, buffer, size / 2);


		
		fr++;

		/* =============================================
		|                  VFD Process
		==============================================*/

		printf("frame[%d]:\n", fr);


		IMP_VFD_Process(hDHandle, frame);
		/* =============================================
						VFD Get result
		==============================================*/

		//IMP_VFD_Release(&hDHandle);
		IMP_VFD_GetResult(hDHandle, &stResult);

		/* =============================================
							VFD draw
		==============================================*/
		vfd_draw(stResult, frame);


		fwrite(frame->pu8D1, size, 1, resultFile);
		fwrite(frame->pu8D2, size / 4, 1, resultFile);
		fwrite(frame->pu8D3, size / 4, 1, resultFile);
	}
	// =============================================
	//                    VFD Release
	// ==============================================
	IMP_VFD_Release(&hDHandle);
	
	//yuv420 memory
	free(frame->pu8D1);
	free(frame); frame = NULL;
	free(buffer); buffer = NULL;
	fclose(yuvFile);
	fclose(resultFile);
	getchar();

	return 0;

}