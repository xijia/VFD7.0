#include "utils.h"
#include<string.h>

void IMP_Draw_Point(IMAGE3_S* image, IMP_POINT32F_S point, int r, const unsigned char * color) {
	float x = point.f32X;
	float y = point.f32Y;
	int h = image->s32H;
	int w = image->s32W;
	float i, j;
	int r2 = r*r;
	for (i = x - r; i < x + r; ++i) {
		for (j = y - r; j < y + r; ++j) {
			if (i > 0 && i < w && j>0 && j < h && ((i - x)*(i - x) + (j - y)*(j - y))<r2) {
				IMP_Draw_a_Point_private(image, i, j, color, 1);
			}
		}
	}
}

void IMP_Draw_Rect(IMAGE3_S* image, IMP_RECT_S rect, int lineWidth, const unsigned char* color) {
	int w = image->s32W;
	int h = image->s32H;
	int x, y, x0, y0;

	y0 = rect.s16Y1;
	for (x = rect.s16X1; x<rect.s16X2 + 1; ++x)
	{
		for (int i = 0; i < lineWidth; ++i) {
			y = y0 + i;
			if (x > 0 && x < w && y>0 && y < h) {
				IMP_Draw_a_Point_private(image, x, y, color, 1);
			}

		}
	}

	y0 = rect.s16Y2;
	for (x = rect.s16X1; x<rect.s16X2 + 1; ++x)
	{
		for (int i = 0; i < lineWidth; ++i) {
			y = y0 + i;
			if (x > 0 && x < w && y>0 && y < h) {
				IMP_Draw_a_Point_private(image, x, y, color, 1);
			}

		}
	}
	x0 = rect.s16X1;
	for (y = rect.s16Y1; y<rect.s16Y2 + 1; ++y)
	{
		for (int i = 0; i < lineWidth; ++i) {
			x = x0 + i;
			if (x > 0 && x < w &&y>0 && y < h) {
				IMP_Draw_a_Point_private(image, x, y, color, 1);
			}

		}
	}
	x0 = rect.s16X2;
	for (y = rect.s16Y1; y<rect.s16Y2 + 1; ++y)
	{
		for (int i = 0; i < lineWidth; ++i) {
			x = x0 + i;
			if (x > 0 && x < w && y>0 && y < h) {
				IMP_Draw_a_Point_private(image, x, y, color, 1);
			}

		}
	}
}


void IMP_Draw_a_Point_private(IMAGE3_S* image, int x, int y, const unsigned char* color, const unsigned char channel_mask) {
	int w = image->s32W;
	if (image->pu8D1 && channel_mask & 0x01) {
		image->pu8D1[y*w + x] = color[0];
	}
	if (image->pu8D2 && channel_mask & 0x02) {
		//image->pu8D2[y*w + x] = color[1];
	}
	if (image->pu8D3  && channel_mask & 0x03) {
		//image->pu8D3[y*w + x] = color[2];
	}
}





IMP_VOID DrawTextInVbuf(IMAGE3_S* image, int x, int y,
	int scale, char *text, int ylineadd, const unsigned char *fontlib, int charw, int charh)
{
	char * pVBufVirt_Y;
	//char * pVBufVirt_C;
	char * pMemContent;

	int i, j, k, m;

	pVBufVirt_Y = image->pu8D1;
	i = 0;
	int liney, linex;
	linex = x;
	liney = y;
	int xcharwidth = charw*scale;
	int ycharheight = charh*scale;
	int interlace = ylineadd*scale;
	int index;
	int offset;
	//printf("kaishi\n");
	while (text[i] != '\0')
	{
		liney = y;
		switch (text[i])
		{
		case '\n':
			linex = x;
			y = y + ycharheight + interlace;
			break;
		case ' ':
			linex = linex + xcharwidth;
			break;
		default:
			linex = linex + xcharwidth;
			index = text[i];
			offset = index*charh*charw / 8;
			//printf("1111\n");
			pMemContent = pVBufVirt_Y + (liney * image->s32W + linex);
			for (j = 0; j<charh; j++)
			{
				pMemContent = pMemContent + (scale * image->s32W);
				for (k = 0; k<charw; k++)
				{
					if (fontlib[offset + j] & (0x80 >> k))
					{
						for (m = 0; m<scale; m++)
						{
							memset(pMemContent + (k*scale + m * image->s32W), 0x0f, scale);//memset(pMemContent+j*scale*pVBuf->u32Stride[0]+k*scale+m*pVBuf->u32Stride[0],255,scale);

						}

					}
				}
			}
			break;

		}
		i++;
	}
}