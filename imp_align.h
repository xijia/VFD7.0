/***********************************************************************
* FileName: imp_align.h
* Author:  lcy
* Date:  2017/03/31
*
* Description: structures and functions for alignment calculations
* modify: code review1
***********************************************************************/

#ifndef IMP_ALIGN_H
#define IMP_ALIGN_H

#include"imp_algo_type.h"
#include"imp_align_tab_int.h"
#include<math.h>

#ifdef __cplusplus
extern "C"
{
#endif


typedef struct imp_align_RES_S
{
	IMP_S16 align_success;
	IMP_POINT32F_S f32Shape[NUM_LANDMARK];
}IMP_ALIGN_RES_S;


/*-------------------------------------------------------------------
* Function: imp_align_pred_shape
* Description: get predicted shape of given image
* Parameters:
*	image : IMAGE3_S type pointer refer to a YUV image, as input image
*			for alignment
*   roi: IMP_RECT_S type, boundingBox detected by Pico_Face_detecction
*   res: IMP_Align_RES_S type pointer for alignment results
*
--------------------------------------------------------------------*/

IMP_S32 imp_align_pred_shape(
	IMAGE3_S* image, 
	IMP_RECT_S* roi,
	IMP_ALIGN_RES_S* res);

#ifdef __cplusplus
}
#endif

#endif /* IMP_ALIGH_H */

