/***********************************************************************
* FileName: imp_faceQA.h
* Author:  lcy
* Date�� 2017031
* Description: face quality assessment
*
* log: blur, yaw,roll,pitch(only support for up) evaluations
* modify: code review1
***********************************************************************/

#ifndef IMP_FACEQA_H	
#define IMP_FACEQA_H

#include"imp_align.h"
#include<stdlib.h>

#ifdef __cplusplus
extern "C"
{
#endif
	float evaluate_yaw(IMP_POINT32F_S* f32Shape);
	float evaluate_roll(IMP_POINT32F_S* f32Shape);
	float evaluate_blur(IMAGE3_S* image, IMP_RECT_S* roi);
	int evaluate_pitch(IMP_POINT32F_S* f32Shape);
/***********************************************************************
* Function: imp_evaluate_faceQA
* Description: face quality assessment 
* Parameters:
*	image : IMAGE3_S type pointer refer to a YUV image, as input image
*   roi: IMP_RECT_S type pointer, boundingBox of face detected by Pico
*   res: IMP_Align_RES_S type pointer for alignment results
*   QAlevel: integer type: 0,1,2,3,4,5
*           0 for close faceQA
*           the higner QAlevel, the higner face quality
* Return: an integer score for faceQA:
*         -1 for no face quality assessment
*         0 for bad quality
*         1 for good quality
*
* Example: 
*        IMAGE3_S* yuvimage=imread(imagename);
*        IMP_RECT_S roi;
*		  roi.s16X1=10; 
*         roi.s16Y1=30;  
*		  roi.s16X2=150;
*         roi.s16Y2=170;  
*        IMP_Align_RES_S res;
*        res.align_success=0;
*        int QA_level=3;
*        int faceQuality= evaluate_faceQA(yuvimage,&roi,&res,QA_level);
***********************************************************************/
int evaluate_faceQA(IMAGE3_S* image,
		IMP_RECT_S* roi,
		IMP_ALIGN_RES_S* res,
	int QA_level);


#ifdef __cplusplus
}
#endif

#endif /* IMP_FACEQA_H */