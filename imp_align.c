/***********************************************************************
* FileName: imp_align.c
* Author:  lcy
* Date:  2017/03/31
*
* Description: structures and functions for shape calculations
*
* modify: code review1
***********************************************************************/


#include"imp_align.h"

#define LEFT_LEAF_IDX(x) (x+x+1) 
#define RIGHT_LEAF_IDX(x) (x+x+2)

typedef struct imp_align_ROI_S
{
	IMP_S16 x1;
	IMP_S16 y1;
	IMP_S16 x2;
	IMP_S16 y2;
	IMP_S16 w;
	IMP_S16 h;
}IMP_ALIGN_ROI_S;
/*-------------------------------------------------------------------
* Function: mul_AB
* Description: matrix multiplication C = A*B
*
* Input:	A [rowsA * colsA_rowsB]
*			B [colsA_rowsB * colsB]
*			rowsA:			row of A, row of C
*			colsA_rowsB:	col of A, col of B
*			colsB:			row of B, col of C
* Output:	C [rowsA * colsB]
*
* Example: float A[4] = { 1, 2, 3, 4 };
*          float B[4] = { 2, 3, 4, 5 };
*		   float C[4] = { 0, 0, 0, 0 };
*          ABdot(A,B,2,2,2,C);
*
*          | 1  2 | * | 2  3 |  =  | 10  13 |
*		   | 3  4 |   | 4  5 |     | 22  29 |
*
*		   C={ 10,13,22,29};
-------------------------------------------------------------------*/
void mul_AB(const float* A, const float*B,int rowsA, 
	int colsA_rowsB,int colsB, float* C)
{
	float r = 1.f;
	int i, k, j, temp1, temp2, temp3;

	temp1 = -colsA_rowsB;
	temp2 = -colsB;

	for (i = 0; i < rowsA; ++i)
	{
		temp1 += colsA_rowsB;
		temp2 += colsB;
		temp3 = -colsB;
		for (k = 0; k < colsA_rowsB; ++k)
		{
			//r = A[i*colsA_rowsB + k];
			temp3 += colsB;
			r = A[temp1 + k];
			for (j = 0; j < colsB; ++j)
			{
				//C[i*colsB + j] += r* B[k*colsB + j];
				C[temp2 + j] += r* B[temp3 + j];
			}
		}
	}
}

/*------------------------------------------------------------------
* Function: mul_ABt
* Description: matrix multiplication Ct=A*Bt
*
* Input: A [rowsA * colsA_colsB]
*        B [rowsB * colsA_colsB]
*        rowsA:			row of A, col of C
*        colsA_colsB:	col of A, col of B
*        rowsB:			row of B, row of C
* Output: C [rowsB * rowsA]
------------------------------------------------------------------*/
void mul_ABt(const float* A, const float*B, int rowsA, 
	int colsA_colsB, int rowsB, float* C) 
{
	float r = 1.f;
	int i, k, j, temp1, temp2,temp3;

	temp1 = -rowsA;
	temp2 = -colsA_colsB;

	for (i = 0; i < rowsB; ++i)
	{
		temp1 += rowsA;
		temp2 += colsA_colsB;
		temp3 = -colsA_colsB;
		for (k = 0; k < rowsA; ++k) 
		{
			temp3 += colsA_colsB;
			//r = B[i*rowsA + k];
			r = B[temp1 + k];
			for (j = 0; j <colsA_colsB; ++j) 
			{
				//C[i*colsA_colsB + j] += r* A[k*colsA_colsB + j];
				C[temp2 + j] += r* A[temp3 + j];
			}
		}
	}
}

/*------------------------------------------------------------------
* Function: mul_AtB
* Description: matrix multiplication C=At*B
*
* Input: A [rowsA_rowsB * colsA]
*        B [rowsA_rowsB * colsB]
*        colsA: col of A, row of C
*        rowsA_rowsB: row of A, row of B
*        colsB: col of B, col of C
* Output: C [colsA,colsB]
-------------------------------------------------------------------*/
void mul_AtB(const float* A, const float*B, int colsA, 
	int rowsA_rowsB, int colsB, float* C)
{
	float r = 1.f;
	int i, k, j, temp1, temp2,temp3;

	temp1 = -colsB;
	temp2 = -colsA;

	for (k = 0; k < rowsA_rowsB; ++k) 
	{
		temp2 += colsA;
		temp1 += colsB;
		temp3 = -colsB;
		for (i = 0; i < colsA; ++i) 
		{
			//r = A[k*n + i];
			r = A[temp2 + i];
			temp3 += colsB;
			for (j = 0; j < colsB; ++j) 
			{
				//C[i*colsB + j] += r* B[k*colsB + j];
				C[temp3 + j] += r* B[temp1 + j];

			}
		}
	}
}

/*-------------------------------------------------------------------
* Function: svd_22
* Description: 2x2 matrix SVD decomposition
*
* Input: A [2x2]
* Output: U,Vt,S
-------------------------------------------------------------------*/
int svd_22(const float*A, float*U, float*Vt, float*S) 
{
	// AtA  =At * A
	float temp;
	float AtA[4] = { 0,0,0,0 };
	float Sinv[4] = { 0,0,0,0 };
	float V[4] = { 0,0,0,0 };
	float VSinv[4] = { 0, 0, 0, 0 };
	float delta = 0;
	float lamda1, lamda2, scale_1, scale_2, norm_1, norm_2;

	temp = A[0] * A[1] + A[2] * A[3];
	AtA[0] = A[0] * A[0] + A[2] * A[2];
	AtA[1] = temp;
	AtA[2] = temp;
	AtA[3] = A[1] * A[1] + A[3] * A[3];
	// AtA always symmetric, so the eigen values are always real.
	// x^2  - (a[0]+a[3])x + a[0]*a[3]-a[1]*a[1] =0 
	delta = (float)(sqrt((AtA[0] + AtA[3])*(AtA[0] + AtA[3]) 
		- 4 * (AtA[0] * AtA[3] - AtA[1] * AtA[1])));
	lamda1 = (float) ((AtA[0] + AtA[3] + delta) *0.5);
	lamda2 =lamda1-delta;
	S[0] = (float)sqrt(lamda1);
	S[1] = 0;
	S[2] = 0;
	S[3] =(float) sqrt(lamda2);
	Sinv[3] =(float)( 1. / S[3]);
	Sinv[0] =(float)( 1. / S[0]);

	scale_1 = (lamda1 - AtA[0]) / AtA[1];
	scale_2 = (lamda2 - AtA[0]) / AtA[1];

	norm_1 = (float)sqrt(1 + scale_1*scale_1);
	norm_2 = (float)sqrt(1 + scale_2 * scale_2);

	V[0] = 1 / norm_1;
	V[1] = 1 / norm_2;
	V[2] = V[1];
	V[3] = -V[0];
	//V[2] = scale_1*V[0];
	//V[3] = scale_2*V[1];

	Vt[0] = V[0];
	Vt[1] = V[2];
	Vt[2] = V[1];
	Vt[3] = V[3];
	// U=AVSinv
  
	mul_AB(V, Sinv, 2, 2, 2, VSinv);
	mul_AB(A, VSinv, 2, 2, 2, U);

	return IMP_SUCCESS;
}


/*-------------------------------------------------------------------
* Function: get_cov
* Description: get covariance matrix of 2 shapes
*
* Input: pred_shape
*        mean_shape
* Output: sigma: variance
cov: covariance matrix
-------------------------------------------------------------------*/
int get_cov(float* pred_shape,
	const float* mean_shape,
	float* sigma,
	float* cov) 
{
	float pred_temp[NUM2];
	float mean_temp[NUM2];

	float mx1 = 0.f;
	float my1 = 0.f;
	float mx2 = 0.f;
	float my2 = 0.f;

	int i;

	for (i = 0; i < NUM_LANDMARK; i++) 
	{
		mx1 += pred_shape[i + i];
		my1 += pred_shape[i + i + 1];
		mx2 += mean_shape[i + i];
		my2 += mean_shape[i + i + 1];

	}

	mx1 *= NUM_LMinv;
	my1 *= NUM_LMinv;
	mx2 *= NUM_LMinv;
	my2 *= NUM_LMinv;

	for (i = 0; i < NUM_LANDMARK; i++) 
	{
		pred_temp[i + i] = pred_shape[i + i] - mx1;
		pred_temp[i + i + 1] = pred_shape[i + i + 1] - my1;
		(*sigma) = (*sigma) + pred_temp[i + i] * pred_temp[i + i] 
			+ pred_temp[i + i + 1] * pred_temp[i + i + 1];
		mean_temp[i + i] = mean_shape[i + i] - mx2;
		mean_temp[i + i + 1] = mean_shape[i + i + 1] - my2;
	}

	//(*sigma) = (*sigma) / (float)NUM_LANDMARK;
	mul_AtB(pred_temp, mean_shape, 2, NUM_LANDMARK, 2, cov);

	cov[0] *= NUM_LMinv;
	cov[1] *= NUM_LMinv;
	cov[2] *= NUM_LMinv;
	cov[3] *=NUM_LMinv;

	return IMP_SUCCESS;
}

/*-------------------------------------------------------------------
* Function: unnormalizing_shape
* Description: unnormalize shape
* Input: rect
*        shape_normed: shape normed in [-1,1]*[-1,1]
* Output: shape_unnorm
-------------------------------------------------------------------*/
int unnormalizing_shape(
	float* shape_normed,
	float* shape_unnorm,
	IMP_ALIGN_ROI_S* rect) 
{
	int i;
	for (i = 0; i < NUM_LANDMARK; i++)
	{
		shape_unnorm[i+i] = shape_normed[i+i] * rect->w + rect->x1;
		shape_unnorm[i+i+1] = shape_normed[i+i+1] * rect->h + rect->y1;
	}
	return IMP_SUCCESS;
}

/*-------------------------------------------------------------------
* Function: extract_pixel_values
* Description: extract pixel_values from an image
* Input: image
*        shape_normed: face landmarks normed in [-1,1]*[-1,1]
*        tform_inv: inverse of transform matrix
*        deltas: (delta_x,delta_y)
rect
* Output: pixel_values
-------------------------------------------------------------------*/
int extract_pixel_values(
	unsigned char* pixel_values,
	IMAGE3_S* image,
	float* shape_normed,
	float* tform_inv,
	const float* deltas,
	IMP_ALIGN_ROI_S* rect)
{
	float deltas_unnormed[NUM_PATTERNS + NUM_PATTERNS];
	int i, j, x, y;
	int idx = 0;
	for (i = 0; i < NUM_PATTERNS; ++i)
	{
		// new_delta = tform_inv * deltas[i];
		deltas_unnormed[i + i] = deltas[i + i] * tform_inv[0] + deltas[i + i + 1] * tform_inv[2];
		deltas_unnormed[i + i + 1] = deltas[i + i] * tform_inv[1] + deltas[i + i + 1] * tform_inv[3];

	}
	for (i = 0; i <NUM_LANDMARK; ++i)
	{
		for (j = 0; j < NUM_PATTERNS; ++j)
		{
			x = (int)((deltas_unnormed[j + j] + shape_normed[i + i]) * rect->w + rect->x1 + 0.5f);
			y = (int)((deltas_unnormed[j + j + 1] + shape_normed[i + i + 1])* rect->h + rect->y1 + 0.5f);
			x = x > 0 ? x : 0;
			y = y > 0 ? y : 0;
			x = rect->x2  > x ? x : rect->w;   // rect.x2
			y = rect->y2 > y ? y : rect->h;   // rect.y2
			pixel_values[idx]=image->pu8D1[x + image->s32W*y];
			//pixel_values[idx] = image.at<uchar>(y, x);
			idx++;
		}
	}  
	return 0;
}


/*-------------------------------------------------------------------
* Function: find_similarity_tform
* Description: find similarity transform matrix
* Input: shape_pred: predicted_shape
*        shape_mean: mean_shape
* Output: tform: transform matrix
*         tform_inv: inverse of transform matrix
-------------------------------------------------------------------*/
int find_similarity_tform(
	float* shape_pred,
	const float* mean_shape,
	float* tform,
	float* tform_inv
) {
	float  sigma = 0.f;
	float cov[4] = { 0, 0, 0, 0 };
	float u[4] = { 0,0,0,0 };
	float S4[4] = { 0,0,0,0 };
	float v[4] = { 0, 0, 0, 0 };
	float temp[4] = { 0,0,0,0 };
	float r[4] = { 0,0,0,0 };
	float det_tfrom;
	float constant = 1.0f;
	float S[4] = { 1.f,0,0, 1.f };
	float detu, detv, detcov;

	get_cov(shape_pred, mean_shape, &sigma, cov);
	//SVD :covA=USVt
	svd_22(cov, v, u, S4);

	detu = -u[0] * u[0] - u[1] * u[1];
	detv = -v[0] * v[0] - v[1] * v[1];
	detcov = cov[0] * cov[3] - cov[1] * cov[2];

	
	if (detcov < 0.0f || (detcov == 0 && detu*detv < 0))
	{
		if (S4[3] < S4[0]) S[3] = -1.0f;
		else S[0] = -1.0f;
	}
	// tform = c*U*S*V
	if (sigma != 0) {
		constant = NUM_LANDMARK / sigma * (S[0] * S4[0] + S[3] * S4[3]);
	}

	mul_AB(u, S, 2, 2, 2, temp);
	mul_AB(temp, v, 2, 2, 2, r);

	tform[0] = constant * r[0];
	tform[1] = constant * r[2];
	tform[2] = constant * r[1];
	tform[3] = constant * r[3];
		
	// tform_inv 
	det_tfrom =(float)(1./ (tform[0] * tform[3] - tform[1] * tform[2]));
	tform_inv[0] = tform[3] * det_tfrom;
	tform_inv[1] = -tform[1]* det_tfrom;
	tform_inv[2] = -tform[2]* det_tfrom;
	tform_inv[3] = tform[0] * det_tfrom;		
	return 0;
}

/*-------------------------------------------------------------------
* Function: which_leaf
* Description: get leaf_index for a given image
* Input: pixel_values ( of an image)
*        i:  the i_th cascade (index begin with 0)
*        j:  the j_th tree ((index begin with 0)
* Return: leaf_index
-------------------------------------------------------------------*/
int which_leaf(const unsigned char* pixel_values, int i, int j) {
	int k = 0;
	int temp=0;
	while (k < NUM_SPLIT_NODE)
	{
		temp=(pixel_values[IDX1[i][j][k]] - pixel_values[IDX2[i][j][k]])<< MULTIPLY;
		if ( temp> THRES[i][j][k])
			k = LEFT_LEAF_IDX(k);
		else
			k = RIGHT_LEAF_IDX(k);
	}
	return k - NUM_SPLIT_NODE;
}

/*-------------------------------------------------------------------
* Function: imp_align_pred_shape
* Description: get predicted shape of given image
* Parameters:
*	image : IMAGE3_S type pointer refer to a YUV image, as input image
*			for alignment
*   roi: IMP_RECT_S type, boundingBox detected by Pico_Face_detecction
*   res: IMP_Align_RES_S type pointer for alignment results
*
--------------------------------------------------------------------*/
IMP_S32 imp_align_pred_shape(IMAGE3_S* image, 
	IMP_RECT_S* roi, 
	IMP_ALIGN_RES_S* res) 
{
	int i, j, k, p, leaf_id;
	IMP_ALIGN_ROI_S rect;
	int  roi_w, roi_h;
	unsigned char pixel_values[NUM_LANDMARK*NUM_PATTERNS];
	float tform[4] = { 0, 0, 0, 0 };
	float tform_inv[4] = { 0, 0, 0, 0 };
	float shapepred1[NUM2] = { 0.f };
	float shapepred[NUM2] = { 0.f };

	roi_w = roi->s16X2 - roi->s16X1;
	roi_h = roi->s16Y2 - roi->s16Y1;
	rect.x1 = 0;
	rect.y1 = 0;
	rect.w = 0;
	rect.h = 0;
	if (NUM_LANDMARK == 5) 
	{
		rect.x1 = (int)(roi->s16X1 - roi_w / 8.);
		rect.y1 = (int)(roi->s16Y1 - roi_h / 8.);
		rect.w = (int)(roi_w*5. / 4);
		rect.h = (int)(roi_h*5. / 4);
	}
	else if (NUM_LANDMARK == 9) 
	{
		rect.x1 = (int)(roi->s16X1 - roi_w / 4.);
		rect.y1 = (int)(roi->s16Y1 - roi_h / 4.);
		rect.w= (int)(roi_w * 3. / 2);
		rect.h = (int)(roi_h * 3. / 2);
	}
	else   
	{
		rect.x1 = (int)(roi->s16X1 - roi_w / 3.);
		rect.y1 = (int)(roi->s16Y1 - roi_h / 3.);
		rect.w = (int)(roi_w * 5. / 3);
		rect.h = (int)(roi_h * 5. / 3);
	}

	rect.x2 = rect.x1 + rect.w;
	rect.y2 = rect.y1 + rect.h;

	// region verify
	if (rect.x1< 0 || rect.y1 < 0 
		|| rect.x2 >= image->s32W 
		|| rect.y2 >= image->s32H) 
	{
		res->align_success = 0;
		return IMP_FAILURE;
	}

	// initial shape with mean_shape
	for (k = 0; k < NUM2; ++k) 
	{
		shapepred[k] = MEAN_SHAPE[k];
	}

	for (i = 0; i < NUM_CASCADE; i++)
	{
		tform[0] = 0;
		tform[1] = 0;
		tform[2] = 0;
		tform[3] = 0;
		tform_inv[0] = 0;
		tform_inv[1] = 0;
		tform_inv[2] = 0;
		tform_inv[3] = 0;

		find_similarity_tform(shapepred, MEAN_SHAPE, tform, tform_inv);

		extract_pixel_values(pixel_values, image, shapepred,
			tform_inv, DELTAS,&rect);
		

		for (p = 0; p < NUM2; p++) {
			shapepred1[p] = 0;
		}

		mul_ABt(tform, shapepred, 2, 2, NUM_LANDMARK, shapepred1);

		for (j = 0; j < NUM_TREE; j++)
		{
			//int leaf_id = cascades[i][j](pixel_values);
			leaf_id = which_leaf(pixel_values, i, j);

			for (k = 0; k < NUM_LANDMARK; ++k) {
				shapepred1[k + k] += LEAFVAL[i][j][leaf_id][k][0];
				shapepred1[k + k + 1] += LEAFVAL[i][j][leaf_id][k][1];
			}

		}

		for (k = 0; k < NUM2; ++k) shapepred[k] = 0.f;
		//shape_pred = tform_inv*shape_pred;
		mul_ABt(tform_inv, shapepred1, 2, 2, NUM_LANDMARK, shapepred);

	}
	for (p = 0; p < NUM2; p++) {
		shapepred1[p] = 0;
	}
	unnormalizing_shape(shapepred, shapepred1,&rect);
	for (p = 0; p < NUM_LANDMARK; p++) {
		res->f32Shape[p].f32X = shapepred1[p + p];
		res->f32Shape[p].f32Y = shapepred1[p + p+1];
	}
	res->align_success = 1;
	return  IMP_SUCCESS; 
}

